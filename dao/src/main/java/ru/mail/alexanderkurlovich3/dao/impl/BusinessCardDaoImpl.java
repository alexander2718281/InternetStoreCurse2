package ru.mail.alexanderkurlovich3.dao.impl;


import org.springframework.stereotype.Repository;
import ru.mail.alexanderkurlovich3.dao.BusinessCardDao;
import ru.mail.alexanderkurlovich3.dao.model.BusinessCard;

@Repository
public class BusinessCardDaoImpl extends GenericDaoImpl<BusinessCard> implements BusinessCardDao {

    public BusinessCardDaoImpl() {
        super(BusinessCard.class);
    }
}
