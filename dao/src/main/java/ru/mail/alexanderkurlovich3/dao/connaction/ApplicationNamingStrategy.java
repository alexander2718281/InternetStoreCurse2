package ru.mail.alexanderkurlovich3.dao.connaction;


import org.apache.commons.lang3.StringUtils;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
public class ApplicationNamingStrategy implements PhysicalNamingStrategy {

    @Override
    public Identifier toPhysicalCatalogName(Identifier identifier, JdbcEnvironment jdbcEnvironment) {
        return identifier;
    }

    @Override
    public Identifier toPhysicalSchemaName(Identifier identifier, JdbcEnvironment jdbcEnvironment) {
        return identifier;
    }

    @Override
    public Identifier toPhysicalTableName(Identifier identifier, JdbcEnvironment jdbcEnvironment) {
        return getIdentifier(identifier, jdbcEnvironment, "T_");
    }

    @Override
    public Identifier toPhysicalSequenceName(Identifier identifier, JdbcEnvironment jdbcEnvironment) {
        return identifier;
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier identifier, JdbcEnvironment jdbcEnvironment) {
        switch (identifier.getText()) {
            case "product_id":
                return jdbcEnvironment.getIdentifierHelper().toIdentifier("F_PRODUCT_ID");
            case "user_id":
                return jdbcEnvironment.getIdentifierHelper().toIdentifier("F_USER_ID");
            case "role_id":
                return jdbcEnvironment.getIdentifierHelper().toIdentifier("F_ROLE_ID");
            case "discount_id":
                return jdbcEnvironment.getIdentifierHelper().toIdentifier("F_DISCOUNT_ID");
        }
        return getIdentifier(identifier, jdbcEnvironment, "F_");
    }

    private Identifier getIdentifier(Identifier name, JdbcEnvironment jdbcEnvironment, String prefix) {
        final List<String> parts = splitAndReplace(name.getText());
        return jdbcEnvironment.getIdentifierHelper().toIdentifier(join(parts, prefix), name.isQuoted());
    }

    private List<String> splitAndReplace(String name) {
        List<String> result = new ArrayList<>();
        for (String part : StringUtils.splitByCharacterTypeCamelCase(name)) {
            if (part == null || part.trim().isEmpty()) {
                continue;
            }
            result.add(part.toUpperCase(Locale.ROOT));
        }
        return result;
    }

    private String join(List<String> parts, String prefix) {
        boolean firstPass = true;
        String separator = "";
        StringBuilder joined = new StringBuilder();
        joined.append(prefix);
        for (String part : parts) {
            joined.append(separator).append(part);
            if (firstPass) {
                firstPass = false;
                separator = "_";
            }
        }
        return joined.toString();
    }
}
