package ru.mail.alexanderkurlovich3.dao.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DatabaseProperties {
    private final Environment environment;

    @Autowired
    public DatabaseProperties(Environment environment) {
        this.environment = environment;
    }

    private String databaseDriverName;
    private String databaseUrl;
    private String databaseUsername;
    private String databasePassword;

    private String maxPoolSize;
    private String dataSourceClass;
    private String cachePreparedStatements;
    private String cachePreparedStatementsSize;
    private String cachePreparedStatementsSQLLimit;
    private String useServerPreparedStatements;

    private String hbm2ddlAuto;
    private String currentSessionContextClass;
    private String dialect;
    private String showSQL;
    private String secondLevelCache;
    private String factoryClass;

    @PostConstruct
    public void initialize() {
        this.databaseDriverName = environment.getProperty("database.driver.name");
        this.databaseUrl = environment.getProperty("database.url");
        this.databaseUsername = environment.getProperty("database.username");
        this.databasePassword = environment.getProperty("database.password");
        this.hbm2ddlAuto = environment.getProperty("hibernate.hbm2ddl.auto");
        this.currentSessionContextClass = environment.getProperty("hibernate.current_session_context_class");
        this.maxPoolSize = environment.getProperty("pool.max.size");
        this.dataSourceClass = environment.getProperty("pool.data.source.class");
        this.cachePreparedStatements = environment.getProperty("pool.cache.prepared.statement");
        this.cachePreparedStatementsSize = environment.getProperty("pool.cache.prepared.statement.size");
        this.cachePreparedStatementsSQLLimit = environment.getProperty("pool.cache.prepared.statement.sql.limit");
        this.useServerPreparedStatements = environment.getProperty("pool.use.server.prepared.statements");
        this.dialect = environment.getProperty("hibernate.dialect");
        this.showSQL = environment.getProperty("hibernate.show_sql");
        this.secondLevelCache = environment.getProperty("hibernate.cache.use_second_level_cache");
        this.factoryClass = environment.getProperty("hibernate.cache.region.factory_class");

    }

    public String getCachePreparedStatementsSize() {
        return cachePreparedStatementsSize;
    }

    public String getCachePreparedStatementsSQLLimit() {
        return cachePreparedStatementsSQLLimit;
    }

    public String getUseServerPreparedStatements() {
        return useServerPreparedStatements;
    }

    public String getDialect() {
        return dialect;
    }

    public String getShowSQL() {
        return showSQL;
    }

    public String getSecondLevelCache() {
        return secondLevelCache;
    }

    public String getFactoryClass() {
        return factoryClass;
    }

    public String getDatabaseDriverName() {
        return databaseDriverName;
    }

    public String getDatabaseUrl() {
        return databaseUrl;
    }

    public String getDatabaseUsername() {
        return databaseUsername;
    }

    public String getDatabasePassword() {
        return databasePassword;
    }

    public String getHbm2ddlAuto() {
        return hbm2ddlAuto;
    }

    public String getCurrentSessionContextClass() {
        return currentSessionContextClass;
    }

    public String getMaxPoolSize() {
        return maxPoolSize;
    }

    public String getDataSourceClass() {
        return dataSourceClass;
    }

    public String getCachePreparedStatements() {
        return cachePreparedStatements;
    }
}
