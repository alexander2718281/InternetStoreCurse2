package ru.mail.alexanderkurlovich3.dao.model;

public enum OrderStatus {
    NEW,
    REVIEWING,
    IN_PROGRESS,
    DELIVERED
}
