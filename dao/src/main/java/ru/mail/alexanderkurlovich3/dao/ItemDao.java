package ru.mail.alexanderkurlovich3.dao;


import ru.mail.alexanderkurlovich3.dao.model.Item;

import java.util.List;

public interface ItemDao extends GenericDao<Item> {

    Item getByNumber(String number);

    List<Item> getItemsInPage(int startResult, int fields);

}
