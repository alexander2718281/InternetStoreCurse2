package ru.mail.alexanderkurlovich3.dao;

import ru.mail.alexanderkurlovich3.dao.model.Audit;

import java.util.List;

public interface AuditDao extends GenericDao<Audit>{

    List<Audit> getAuditsInPage(int startResult, int fields);
}
