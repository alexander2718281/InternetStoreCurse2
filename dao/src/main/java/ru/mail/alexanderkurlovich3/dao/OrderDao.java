package ru.mail.alexanderkurlovich3.dao;


import ru.mail.alexanderkurlovich3.dao.model.Item;
import ru.mail.alexanderkurlovich3.dao.model.Order;
import ru.mail.alexanderkurlovich3.dao.model.User;

import java.util.List;

public interface OrderDao extends GenericDao<Order>{

    Order getNewOrderByIds(long productId, long userID);

    Order getNewOrderByIds(Item item, User user);

    List<Order> getOrdersOfUser(User user);

    List<Order> getUndeliveredOrdersInPage(int startResult, int fields);

}
