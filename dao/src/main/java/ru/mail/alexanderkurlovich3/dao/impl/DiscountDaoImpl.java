package ru.mail.alexanderkurlovich3.dao.impl;


import org.springframework.stereotype.Repository;
import ru.mail.alexanderkurlovich3.dao.DiscountDao;
import ru.mail.alexanderkurlovich3.dao.model.Discount;

@Repository
public class DiscountDaoImpl extends GenericDaoImpl<Discount> implements DiscountDao{

    public DiscountDaoImpl() {
        super(Discount.class);
    }


}
