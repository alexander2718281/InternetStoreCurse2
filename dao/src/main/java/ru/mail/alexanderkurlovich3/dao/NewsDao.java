package ru.mail.alexanderkurlovich3.dao;


import ru.mail.alexanderkurlovich3.dao.model.News;

import java.util.List;

public interface NewsDao extends GenericDao<News>{

    List<News> getNewsInPage(int startResult, int fields);
}
