package ru.mail.alexanderkurlovich3.dao.impl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.alexanderkurlovich3.dao.CommentDao;
import ru.mail.alexanderkurlovich3.dao.model.Comment;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CommentDaoImpl extends GenericDaoImpl<Comment> implements CommentDao {

    public CommentDaoImpl() {
        super(Comment.class);
    }

    @Override
    public List<Comment> getCommentsOnPage(Long newsId, int startOfPage, int countOfCommentsOnPage) {
        String hql = "from Comment co where co.news.id=:newsId ORDER BY co.created desc";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("newsId", newsId);
        query.setFirstResult(startOfPage);
        query.setMaxResults(countOfCommentsOnPage);
        return query.list();
    }
}
