package ru.mail.alexanderkurlovich3.dao.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.alexanderkurlovich3.dao.ItemDao;
import ru.mail.alexanderkurlovich3.dao.model.Item;

import java.math.BigDecimal;
import java.util.List;

@Repository
public class ItemDaoImpl extends GenericDaoImpl<Item> implements ItemDao {

    public ItemDaoImpl() {
        super(Item.class);
    }

    @Override
    public Item getByNumber(String number) {
        String hql = "from Item i where i.uniqueNumber=:number and i.deleted = 0";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("number", number);
        Item item = null;
        item = (Item) query.uniqueResult();
        return item;
    }

    @Override
    public List<Item> getItemsInPage(int startResult, int fields) {
        String hql = "from Item i where i.deleted = 0 ORDER BY i.price asc";
        Query query = getCurrentSession().createQuery(hql);
        query.setFirstResult(startResult);
        query.setMaxResults(fields);
        return query.list();
    }

    @Override
    public Long getCount() {
        Long result;
        String hql = "select count(i) from Item i where i.deleted = 0" ;
        Query query = getCurrentSession().createQuery(hql);
        result = (long) query.uniqueResult();
        return result;
    }

    @Override
    public Item findOne(long entityId) {
        String hql = "from Item i where i.id=:id and i.deleted = 0";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("id", entityId);
        return (Item) query.uniqueResult();
    }
}
