package ru.mail.alexanderkurlovich3.dao;

import ru.mail.alexanderkurlovich3.dao.model.Discount;
import ru.mail.alexanderkurlovich3.dao.model.Item;

import java.util.List;

public interface DiscountDao extends GenericDao<Discount> {

}
