package ru.mail.alexanderkurlovich3.dao;


import ru.mail.alexanderkurlovich3.dao.model.Role;

public interface RoleDao extends GenericDao<Role> {
    Role getRoleByName(String name);
}
