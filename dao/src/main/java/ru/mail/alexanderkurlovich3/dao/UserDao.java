package ru.mail.alexanderkurlovich3.dao;

import ru.mail.alexanderkurlovich3.dao.model.User;

import java.util.List;

public interface UserDao extends GenericDao<User> {

    User getUserByEmail(String email);

    boolean isExists(String email);

    List<User> getUsersInPage(int startResult, int fields);

}
