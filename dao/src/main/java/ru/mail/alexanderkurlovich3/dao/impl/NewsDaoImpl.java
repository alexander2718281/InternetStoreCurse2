package ru.mail.alexanderkurlovich3.dao.impl;


import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.alexanderkurlovich3.dao.NewsDao;
import ru.mail.alexanderkurlovich3.dao.model.News;

import java.util.List;

@Repository
public class NewsDaoImpl extends GenericDaoImpl<News> implements NewsDao {

    public NewsDaoImpl() {
        super(News.class);
    }

    @Override
    public List<News> getNewsInPage(int startResult, int fields) {
        String hql = "from News n ORDER BY n.created desc";
        Query query = getCurrentSession().createQuery(hql);
        query.setFirstResult(startResult);
        query.setMaxResults(fields);
        return query.list();
    }
}
