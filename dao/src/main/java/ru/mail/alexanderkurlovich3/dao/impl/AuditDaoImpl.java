package ru.mail.alexanderkurlovich3.dao.impl;


import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.alexanderkurlovich3.dao.AuditDao;
import ru.mail.alexanderkurlovich3.dao.model.Audit;

import java.util.List;

@Repository
public class AuditDaoImpl extends GenericDaoImpl<Audit> implements AuditDao {

    public AuditDaoImpl() {
        super(Audit.class);
    }

    @Override
    public List<Audit> getAuditsInPage(int startResult, int fields) {
        String hql = "from Audit au ORDER BY au.id desc";
        Query query = getCurrentSession().createQuery(hql);
        query.setFirstResult(startResult);
        query.setMaxResults(fields);
        return query.list();
    }
}
