package ru.mail.alexanderkurlovich3.dao.impl;


import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.alexanderkurlovich3.dao.RoleDao;
import ru.mail.alexanderkurlovich3.dao.model.Role;

@Repository
public class RoleDaoImpl extends GenericDaoImpl<Role> implements RoleDao{

    public RoleDaoImpl() {
        super(Role.class);
    }

    @Override
    public Role getRoleByName(String name) {
        String hql = "from Role ro where ro.name=:name";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("name", name);
        Role role = (Role) query.uniqueResult();
        return role;
    }
}
