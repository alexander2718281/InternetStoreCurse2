package ru.mail.alexanderkurlovich3.dao.impl;


import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.alexanderkurlovich3.dao.OrderDao;
import ru.mail.alexanderkurlovich3.dao.model.Item;
import ru.mail.alexanderkurlovich3.dao.model.Order;
import ru.mail.alexanderkurlovich3.dao.model.OrderStatus;
import ru.mail.alexanderkurlovich3.dao.model.User;

import java.util.List;

@Repository
public class OrderDaoImpl extends GenericDaoImpl<Order> implements OrderDao {

    public OrderDaoImpl() {
        super(Order.class);
    }

    @Override
    public Order getNewOrderByIds(long productId, long userID) {
        String hql = "from Order ord where ord.product.id=:productId and ord.user.id=:userId and ord.status=:status";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("productId", productId);
        query.setParameter("userId", userID);
        query.setParameter("status", OrderStatus.NEW);
        Order order = (Order) query.uniqueResult();
        return order;
    }

    @Override
    public Order getNewOrderByIds(Item item, User user) {
        String hql = "from Order ord where ord.item=:item and ord.user=:user and ord.status=:status";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("item", item);
        query.setParameter("user", user);
        query.setParameter("status", OrderStatus.NEW);
        Order order = (Order) query.uniqueResult();
        return order;
    }


    @Override
    public List<Order> getOrdersOfUser(User user) {
        String hql = "from Order ord where ord.user=:user and ord.status!=:status ORDER BY ord.created DESC";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("user", user);
        query.setParameter("status", OrderStatus.DELIVERED);
        List<Order> orders = query.list();
        return orders;
    }

    @Override
    public List<Order> getUndeliveredOrdersInPage(int startResult, int fields) {
        String hql = "from Order ord where ord.status!=:status ORDER BY ord.created DESC";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("status", OrderStatus.DELIVERED);
        query.setFirstResult(startResult);
        query.setMaxResults(fields);
        List<Order> orders = query.list();
        return orders;
    }

    @Override
    public Long getCount() {
        Long result;
        String hql = "select count(ord) from Order ord where ord.status!=:status" ;
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("status", OrderStatus.DELIVERED);
        result = (long) query.uniqueResult();
        return result;
    }
}
