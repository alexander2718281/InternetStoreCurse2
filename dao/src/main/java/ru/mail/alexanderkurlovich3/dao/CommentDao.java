package ru.mail.alexanderkurlovich3.dao;


import ru.mail.alexanderkurlovich3.dao.model.Comment;

import java.util.List;

public interface CommentDao extends GenericDao<Comment> {

    List<Comment> getCommentsOnPage(Long newsId, int startOfPage, int countOfCommentsOnPage);
}
