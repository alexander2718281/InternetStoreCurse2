package ru.mail.alexanderkurlovich3.dao;



import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface GenericDao <T extends Serializable>{

    void create(final T entity);

    T save(final T entity);

    void update(final T entity);

    void delete(final T entity);

    T findOne(final long entityId);

    List<T> getAll();

    Session getCurrentSession();

    Long getCount();
}
