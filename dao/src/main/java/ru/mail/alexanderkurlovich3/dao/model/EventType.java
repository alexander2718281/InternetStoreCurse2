package ru.mail.alexanderkurlovich3.dao.model;

public enum EventType {
    USER,
    PRODUCT,
    ORDER,
    NEWS,
    COMMENTS,
    DISCOUNT
}
