package ru.mail.alexanderkurlovich3.dao.impl;


import org.springframework.stereotype.Repository;
import ru.mail.alexanderkurlovich3.dao.PermisssionDao;
import ru.mail.alexanderkurlovich3.dao.model.Permission;

@Repository
public class PermissionDaoImpl extends GenericDaoImpl<Permission> implements PermisssionDao {

    public PermissionDaoImpl() {
        super(Permission.class);
    }
}
