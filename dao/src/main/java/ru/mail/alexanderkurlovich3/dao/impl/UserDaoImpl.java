package ru.mail.alexanderkurlovich3.dao.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Where;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.alexanderkurlovich3.dao.UserDao;
import ru.mail.alexanderkurlovich3.dao.model.User;

import java.util.List;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

    public UserDaoImpl() {
        super(User.class);
    }

    @Override
    public User getUserByEmail(String email) {
        String hql = "from User u where u.email=:email and u.deleted = 0";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("email", email);
        User user = (User) query.uniqueResult();
        return user;
    }

    @Override
    public boolean isExists(String email) {
        boolean isExists = false;
        String hql = "select u.id from User u where u.email=:email";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("email", email);
        Long res = (Long) query.uniqueResult();
        if (res != null && res != 0) {
            isExists = true;
        }
        return isExists;
    }

    @Override
    public List<User> getUsersInPage(int startResult, int fields) {
        String hql = "from User u where u.deleted = 0 ORDER BY u.id ";
        Query query = getCurrentSession().createQuery(hql);
        query.setFirstResult(startResult);
        query.setMaxResults(fields);
        return query.list();
    }

    @Override
    public User findOne(long entityId) {
        String hql = "from User u where u.id=:id and u.deleted = 0";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter("id", entityId);
        return (User) query.uniqueResult();
    }

    @Override
    public Long getCount() {
        Long result;
        String hql = "select count(u) from User u where u.deleted = 0" ;
        Query query = getCurrentSession().createQuery(hql);
        result = (long) query.uniqueResult();
        return result;
    }
}
