package ru.mail.alexanderkurlovich3.dao;


import ru.mail.alexanderkurlovich3.dao.model.BusinessCard;

public interface BusinessCardDao extends GenericDao<BusinessCard> {
}
