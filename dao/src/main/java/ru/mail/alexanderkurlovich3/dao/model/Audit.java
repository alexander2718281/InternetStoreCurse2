package ru.mail.alexanderkurlovich3.dao.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table
public class Audit implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false, unique = true)
    private Long id;
    @Column
    private String event;
    @Column
    private Date created;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private User user;
    @Enumerated(EnumType.STRING)
    @Column
    private EventType eventType;

    public Audit() {
    }

    public Audit(String event, Date created, User user, EventType type) {
        this.event = event;
        this.created = created;
        this.user = user;
        this.eventType = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Audit audit = (Audit) o;

        if (id != null ? !id.equals(audit.id) : audit.id != null) return false;
        if (event != null ? !event.equals(audit.event) : audit.event != null) return false;
        if (created != null ? !created.equals(audit.created) : audit.created != null) return false;
        if (user != null ? !user.equals(audit.user) : audit.user != null) return false;
        return eventType == audit.eventType;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (event != null ? event.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Audit{" +
                "id=" + id +
                ", event='" + event + '\'' +
                ", created=" + created +
                ", user=" + user +
                ", eventType=" + eventType +
                '}';
    }
}
