package ru.mail.alexanderkurlovich3.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource({"classpath:config.properties", "classpath:addresses.properties"})
@ComponentScan(basePackages = {"ru.mail.alexanderkurlovich3.dao", "ru.mail.alexanderkurlovich3.service", "ru.mail.alexanderkurlovich3.controller", "ru.mail.alexanderkurlovich3.config"})
public class AppConfig{

    @Bean
    public static PropertySourcesPlaceholderConfigurer configurer(){
        return new PropertySourcesPlaceholderConfigurer();
    }
}
