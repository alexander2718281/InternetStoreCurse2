package ru.mail.alexanderkurlovich3.model;

public enum PermissiomEnum {
    //Customer
    CREATE_ORDER,
    //READ_NEWS,
    CUSTOMER_PAGE,
    DO_COMMENT,
    MANAGE_BUSINESS_CARD,
    REDACT_PROFILE,
    READ_ITEMS,

    //Security
    AUDIT,
    SECURITY_PAGE,
    USERS_CONTROL,
    //REDACT_PROFILE,


    //API
    API,


    //Seller
    CUD_NEWS,
    READ_NEWS,
    //REDACT_PROFILE,
    SELLER_PAGE,
    //READ_ITEMS
    CUD_ITEMS,
    WORKING_WITH_ORDERS;

}
