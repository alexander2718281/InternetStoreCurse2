package ru.mail.alexanderkurlovich3.controller.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.mail.alexanderkurlovich3.model.UserDTOWithProfile;

@Component
public class ProfileUpdateValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return UserDTOWithProfile.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDTOWithProfile user = (UserDTOWithProfile) target;

        if (user.getAddress() != null) {
            if (user.getAddress().length() > 70) {
                errors.rejectValue("address", "user.address.long");
            }
        }

        if (user.getTelephone() != null) {
            if (user.getTelephone().length() > 70) {
                errors.rejectValue("telephone", "user.telephone.long");
            }
        }

        if (user.getName() != null) {
            if (user.getName().length() > 50) {
                errors.rejectValue("name", "user.name.long");
            }
        }
        if (user.getSurename() != null) {
            if (user.getSurename().length() > 50) {
                errors.rejectValue("surename", "user.surename.long");
            }
        }
    }
}
