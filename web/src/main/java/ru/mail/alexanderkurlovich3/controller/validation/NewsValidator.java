package ru.mail.alexanderkurlovich3.controller.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.mail.alexanderkurlovich3.model.NewsDTO;

@Component
public class NewsValidator implements Validator{

    @Override
    public boolean supports(Class<?> aClass) {
        return NewsDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "title", "news.title.empty");
        ValidationUtils.rejectIfEmpty(errors, "content", "news.content.empty");
        NewsDTO news = (NewsDTO) o;

        if (news.getTitle().length() > 60){
            errors.rejectValue("title", "news.title.long");
        }


        if (news.getContent().length() > 255){
            errors.rejectValue("content", "news.content.long");
        }
    }
}
