package ru.mail.alexanderkurlovich3.controller.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.mail.alexanderkurlovich3.controller.properties.AddressProperties;
import ru.mail.alexanderkurlovich3.controller.validation.BusinessCardValidator;
import ru.mail.alexanderkurlovich3.model.BusinessCardDTO;
import ru.mail.alexanderkurlovich3.service.BusinessCardService;

import java.util.List;

@Controller
@RequestMapping("/web/cards")
public class BusinessCardController {

    private final AddressProperties addressProperties;
    private final BusinessCardService businessCardService;
    private final BusinessCardValidator businessCardValidator;

    @Autowired
    public BusinessCardController(AddressProperties addressProperties, BusinessCardService businessCardService,
                                  BusinessCardValidator businessCardValidator) {
        this.addressProperties = addressProperties;
        this.businessCardService = businessCardService;
        this.businessCardValidator = businessCardValidator;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('MANAGE_BUSINESS_CARD')")
    public String getBusinessCardsForUser(ModelMap modelMap) {
        List<BusinessCardDTO> cards = businessCardService.getCardsForUser();
        modelMap.addAttribute("cards", cards);
        return addressProperties.getBusinessCardsOfUserPagePath();
    }

    @GetMapping(value = "/create")
    @PreAuthorize("hasAuthority('MANAGE_BUSINESS_CARD')")
    public String getCreatingPafe(ModelMap modelMap) {
        BusinessCardDTO card = new BusinessCardDTO();
        modelMap.addAttribute("card", card);
        return addressProperties.getCreateNewBusinessCardPagePath();
    }

    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('MANAGE_BUSINESS_CARD')")
    public String createNewBusinessCard(@ModelAttribute("card") BusinessCardDTO card,
                                        BindingResult result,
                                        ModelMap modelMap) {
        businessCardValidator.validate(card, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("card", card);
            return addressProperties.getCreateNewBusinessCardPagePath();
        } else {
            businessCardService.createNewBusinessCardForUser(card);
        }
        return "redirect:/web/cards";
    }

}
