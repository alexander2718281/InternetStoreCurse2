package ru.mail.alexanderkurlovich3.controller.controllers.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.mail.alexanderkurlovich3.model.BusinessCardDTO;
import ru.mail.alexanderkurlovich3.service.BusinessCardService;

import java.util.List;

@RestController
@RequestMapping("/api/cards")
public class BusinessCardAPIController {

    private final BusinessCardService businessCardService;

    @Autowired
    public BusinessCardAPIController(BusinessCardService businessCardService) {
        this.businessCardService = businessCardService;
    }

    @GetMapping(value = "/user/{id}")
    @PreAuthorize("hasAuthority('MANAGE_BUSINESS_CARD')")
    public List<BusinessCardDTO> getBusinessCardsOfUser(@PathVariable("id") Long id){
        List<BusinessCardDTO> cards = businessCardService.getCardsForUser(id);
        return cards;
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('MANAGE_BUSINESS_CARD')")
    public BusinessCardDTO deleteBusinessCard(@PathVariable("id") Long id){
        BusinessCardDTO cardDTO = businessCardService.deleteBusinessCard(id);
        return cardDTO;
    }
}
