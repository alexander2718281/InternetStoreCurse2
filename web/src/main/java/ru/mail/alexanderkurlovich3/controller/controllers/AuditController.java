package ru.mail.alexanderkurlovich3.controller.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.mail.alexanderkurlovich3.controller.properties.AddressProperties;
import ru.mail.alexanderkurlovich3.model.AuditDTO;
import ru.mail.alexanderkurlovich3.service.AuditService;

import java.util.List;

@Controller
@RequestMapping("/web/audit")
@PreAuthorize("hasAuthority('AUDIT')")
public class AuditController {

    private final AddressProperties addressProperties;
    private final AuditService auditService;

    @Autowired
    public AuditController(AddressProperties addressProperties, AuditService auditService) {
        this.addressProperties = addressProperties;
        this.auditService = auditService;
    }

    @GetMapping
    public String getAudits(){
        return "redirect:/web/audit/1";
    }

    @GetMapping(value = "/{page}")
    public String getAudits(@PathVariable("page") int page, ModelMap modelMap){
        List<Integer> pages = auditService.getPages(page);
        modelMap.addAttribute("pages.properties", pages);
        List<AuditDTO> audits = auditService.getAuditsInPage(page);
        modelMap.addAttribute("audits", audits);
        return addressProperties.getAuditPagePath();
    }


}
