package ru.mail.alexanderkurlovich3.controller.handlers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.mail.alexanderkurlovich3.controller.properties.AddressProperties;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;

@ControllerAdvice
public class AppExceptionHandler {

    private final AddressProperties addressProperties;

    @Autowired
    public AppExceptionHandler(AddressProperties addressProperties) {
        this.addressProperties = addressProperties;
    }

    @ExceptionHandler(ServiceException.class)
    public String serviceExeption(HttpServletRequest request, Exception ex){
        request.setAttribute("exception", ex);
        request.setAttribute("url", request.getRequestURI());
        return addressProperties.getErrorPagePath();
    }


    @ExceptionHandler(UsernameNotFoundException.class)
    public String userNotFoundException(HttpServletRequest request, Exception ex){
        request.setAttribute("exception", ex);
        request.setAttribute("url", request.getRequestURI());
        return addressProperties.getErrorPagePath();
    }

    @ExceptionHandler(Exception.class)
    public String defaultErrorHandler(HttpServletRequest request, Exception ex) {
        request.setAttribute("exception", ex);
        request.setAttribute("url", request.getRequestURI());
        return addressProperties.getErrorPagePath();
    }
}
