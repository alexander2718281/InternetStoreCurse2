package ru.mail.alexanderkurlovich3.controller.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.mail.alexanderkurlovich3.controller.properties.AddressProperties;
import ru.mail.alexanderkurlovich3.controller.validation.CommentValidator;
import ru.mail.alexanderkurlovich3.controller.validation.NewsValidator;
import ru.mail.alexanderkurlovich3.model.CommentDTO;
import ru.mail.alexanderkurlovich3.model.NewsDTO;
import ru.mail.alexanderkurlovich3.service.CommentService;
import ru.mail.alexanderkurlovich3.service.NewsService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/web/news")
public class NewsController {

    private final AddressProperties addressProperties;
    private final NewsService newsService;
    private final CommentService commentService;
    private final NewsValidator newsValidator;
    private final CommentValidator commentValidator;

    @Autowired
    public NewsController(AddressProperties addressProperties, NewsService newsService,
                          CommentService commentService, NewsValidator newsValidator,
                          CommentValidator commentValidator) {
        this.addressProperties = addressProperties;
        this.newsService = newsService;
        this.commentService = commentService;
        this.newsValidator = newsValidator;
        this.commentValidator = commentValidator;
    }


    @GetMapping
    @PreAuthorize("hasAuthority('READ_NEWS')")
    public String getNews() {
        return "redirect:/web/news/1";
    }


    @GetMapping(value = "/{page}")
    @PreAuthorize("hasAuthority('READ_NEWS')")
    public String getNewsOnPage(@PathVariable("page") int page, ModelMap modelMap) {
        List<Integer> pages = newsService.getPages(page);
        modelMap.addAttribute("pages", pages);
        List<NewsDTO> news = newsService.getNewsInPage(page);
        modelMap.addAttribute("news", news);
        return addressProperties.getNewsPagePath();
    }

    @GetMapping(value = "/create")
    @PreAuthorize("hasAuthority('CUD_NEWS')")
    public String getCreationPage(ModelMap modelMap) {
        modelMap.addAttribute("news", new NewsDTO());
        return addressProperties.getCreteNewsPagePath();
    }

    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('CUD_NEWS')")
    public String cteateNews(
            @ModelAttribute("user") NewsDTO news,
            BindingResult result,
            ModelMap modelMap) {
        newsValidator.validate(news, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("news", news);
            return addressProperties.getCreteNewsPagePath();
        } else {
            newsService.createNews(news);
        }
        return "redirect:/web/news";
    }

    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('CUD_NEWS')")
    public String deleteUsers(@PathVariable("id") Long id) {
        newsService.deleteNews(id);
        return "redirect:/web/news";
    }

    @GetMapping(value = "/read/{id}")
    @PreAuthorize("hasAuthority('READ_NEWS')")
    public String getOneNewsWithComments(@PathVariable("id") Long id, ModelMap modelMap, HttpServletRequest request) {
        NewsDTO news = newsService.getNewsById(id);
        modelMap.addAttribute("news", news);
        int currentPage = Integer.parseInt(request.getParameter("page"));
        List<Integer> pages = commentService.getPages(currentPage);
        modelMap.addAttribute("pages", pages);
        List<CommentDTO> comments = commentService.getNewsCommentsInPage(id, currentPage);
        modelMap.addAttribute("comments", comments);
        return addressProperties.getChoosenPagePath();
    }

    @GetMapping(value = "update/{id}")
    @PreAuthorize("hasAuthority('CUD_NEWS')")
    public String getUpdatePage(@PathVariable("id") Long id, ModelMap modelMap) {
        NewsDTO news = newsService.getNewsById(id);
        modelMap.addAttribute("news", news);
        return addressProperties.getUpdateNewsPagePath();
    }

    @PostMapping(value = "update/{newsId}")
    @PreAuthorize("hasAuthority('CUD_NEWS')")
    public String updateNews(@ModelAttribute("news") NewsDTO news,
                             BindingResult result,
                             @PathVariable("newsId") Long newsId,
                             ModelMap modelMap) {
        newsValidator.validate(news, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("news", news);
            return addressProperties.getCreteNewsPagePath();
        } else {
            newsService.updateNews(newsId, news);
        }
        return "redirect:/web/news";
    }

    @GetMapping(value = "/{id}/comment")
    @PreAuthorize("hasAuthority('READ_NEWS')")
    public String getCommentCreatingPage(@PathVariable("id") Long id, ModelMap modelMap) {
        modelMap.addAttribute("newsId", id);
        modelMap.addAttribute("comment", new CommentDTO());
        return addressProperties.getCreateCommentPagePath();
    }

    @PostMapping(value = "{newsId}/comment")
    public String createComment(@ModelAttribute("comment") CommentDTO comment,
                                BindingResult result, @PathVariable("newsId") Long newsId, ModelMap modelMap) {
        commentValidator.validate(comment, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("comment", comment);
            modelMap.addAttribute("newsId", newsId);
            return addressProperties.getCreateCommentPagePath();
        } else {
            commentService.createComment(newsId, comment.getContent());
            return "redirect:/web/news/read/"+newsId+"?page=1";
        }
    }

    @GetMapping(value = "{newsId}/comment/delete")
    public String deleteCommenr(@PathVariable("newsId") Long newsId, HttpServletRequest request){
        Long commentId = Long.valueOf(request.getParameter("comment"));
        commentService.deleteComment(commentId);
        return "redirect:/web/news/read/"+newsId+"?page=1";
    }
}
