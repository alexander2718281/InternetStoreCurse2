package ru.mail.alexanderkurlovich3.controller.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.model.PermissiomEnum;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;

@Component
public class AppSuccessHandler implements AuthenticationSuccessHandler {

    private static final Logger logger = LogManager.getLogger(AppSuccessHandler.class);
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        handle(httpServletRequest, httpServletResponse, authentication);
        clearAuthenticationAttributes(httpServletRequest);
    }

    private void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        String targetUrl = determineTargetUrl(authentication);
        if (response.isCommitted()){
            logger.debug("Response has already been committed. Unable to redirect to {}", targetUrl);
            return;
        }
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    private String determineTargetUrl(Authentication authentication) {
        boolean isCustomer = false;
        boolean isSecurity = false;
        boolean isAPI = false;
        boolean isSeller = false;
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
            if (grantedAuthority.getAuthority().equals(PermissiomEnum.CREATE_ORDER.toString())) {
                isCustomer = true;
                break;
            } else if (grantedAuthority.getAuthority().equals(PermissiomEnum.AUDIT.toString())){
                isSecurity = true;
                break;
            } else if (grantedAuthority.getAuthority().equals(PermissiomEnum.API.toString())){
                isAPI = true;
                break;
            } else if (grantedAuthority.getAuthority().equals(PermissiomEnum.CUD_NEWS.toString())){
                isSeller = true;
                break;
            }
        }
        if (isCustomer){
            return "/web/page/customer";
        }
        else if (isSecurity){
            return "/web/page/security";
        }
        else if (isAPI){
            return "/web/page/api";
        }
        else if (isSeller){
            return "/web/page/seller";
        }
        else {
            throw new IllegalStateException();
        }
    }

    private void clearAuthenticationAttributes(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if (session == null){
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }


}
