package ru.mail.alexanderkurlovich3.controller.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.mail.alexanderkurlovich3.controller.properties.AddressProperties;
import ru.mail.alexanderkurlovich3.controller.validation.ItemValidator;
import ru.mail.alexanderkurlovich3.model.DiscountDTO;
import ru.mail.alexanderkurlovich3.model.ItemDTO;
import ru.mail.alexanderkurlovich3.service.DiscountService;
import ru.mail.alexanderkurlovich3.service.ItemService;
import ru.mail.alexanderkurlovich3.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/web/items")
public class ItemController {

    private final AddressProperties addressProperties;
    private final ItemService itemService;
    private final DiscountService discountService;
    private final OrderService orderService;
    private final ItemValidator itemValidator;

    @Autowired
    public ItemController(AddressProperties addressProperties, ItemService itemService, DiscountService discountService,
                          OrderService orderService, ItemValidator itemValidator) {
        this.addressProperties = addressProperties;
        this.itemService = itemService;
        this.discountService = discountService;
        this.orderService = orderService;
        this.itemValidator = itemValidator;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('READ_ITEMS')")
    public String getItemsPage() {
        return "redirect:/web/items/1";
    }

    @GetMapping(value = "/{page}")
    @PreAuthorize("hasAuthority('READ_ITEMS')")
    public String getItemsPage(@PathVariable("page") int page, ModelMap modelMap) {
        List<Integer> pages = itemService.getPages(page);
        modelMap.addAttribute("pages", pages);
        List<ItemDTO> items = itemService.getItemsInPage(page);
        modelMap.addAttribute("items", items);
        return addressProperties.getItemsPagePath();
    }

    @PostMapping(value = "/update")
    @PreAuthorize("hasAuthority('CUD_ITEMS')")
    public String updateItems(@RequestParam("file") MultipartFile file) {
        itemService.updateProducts(file);
        return "redirect:/web/items";
    }

    @GetMapping(value = "/update")
    @PreAuthorize("hasAuthority('CUD_ITEMS')")
    public String getUpdatePage() {
        return addressProperties.getInputXMLPagePath();
    }

    @PostMapping(value = "/{itemId}/copy")
    @PreAuthorize("hasAuthority('CUD_ITEMS')")
    public String getCopyPagePath(@PathVariable("itemId") Long itemId, HttpServletRequest request) {
        String newUniqueNumber = request.getParameter("number");
        itemService.copyItem(itemId, newUniqueNumber);
        return "redirect:/web/items";
    }

    @GetMapping(value = "/{itemId}/copy")
    @PreAuthorize("hasAuthority('CUD_ITEMS')")
    public String getCopyPagePath(@PathVariable("itemId") Long itemId, ModelMap modelMap) {
        modelMap.addAttribute("itemlId", itemId);
        return addressProperties.getItemCopyPagePath();
    }

    @GetMapping(value = "/{itemId}/discounts")
    @PreAuthorize("hasAuthority('CUD_ITEMS')")
    public String getDiscountsPage(@PathVariable("itemId") Long itemId, ModelMap modelMap) {
        modelMap.addAttribute("itemlId", itemId);
        List<DiscountDTO> discounts = discountService.getActualDiscounts();
        modelMap.addAttribute("discounts", discounts);
        return addressProperties.getCooseDiscountPagePath();
    }

    @GetMapping(value = "/{itemId}/discount")
    @PreAuthorize("hasAuthority('CUD_ITEMS')")
    public String getDiscountsPage(@PathVariable("itemId") Long itemId, HttpServletRequest request) {
        Long discountId = Long.valueOf(request.getParameter("discountId"));
        discountService.addDiscountToItem(itemId, discountId);
        return "redirect:/web/items";
    }

    @GetMapping(value = "/{itemId}/delete")
    @PreAuthorize("hasAuthority('CUD_ITEMS')")
    public String deleteItem(@PathVariable("itemId") Long itemId) {
        itemService.deleteById(itemId);
        return "redirect:/web/items";
    }

    @PostMapping(value = "/{itemId}/order")
    @PreAuthorize("hasAuthority('CREATE_ORDER')")
    public String createOrderToCurrentUser(@PathVariable("itemId") Long itemId, @RequestParam("num") int quantity) {
        orderService.save(itemId, quantity);
        return "redirect:/web/items";
    }

    @GetMapping(value = "/create")
    @PreAuthorize("hasAuthority('CUD_ITEMS')")
    public String createNewItemPage(ModelMap modelMap) {
        modelMap.addAttribute("item", new ItemDTO());
        return addressProperties.getCreateItemPagePath();
    }

    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('CUD_ITEMS')")
    public String createNewItem(@ModelAttribute("item") ItemDTO item,
                                BindingResult result,
                                ModelMap modelMap) {
        itemValidator.validate(item, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("item", item);
            return addressProperties.getCreateItemPagePath();
        } else {
            boolean success = itemService.createNewItem(item);
            if (!success) {
                modelMap.addAttribute("item", item);
                modelMap.addAttribute("massage", "This unique number already exist");
                return addressProperties.getCreateItemPagePath();
            }
        }
        return "redirect:/web/items";
    }
}
