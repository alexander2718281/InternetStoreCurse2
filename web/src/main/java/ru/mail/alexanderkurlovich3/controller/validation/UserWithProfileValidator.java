package ru.mail.alexanderkurlovich3.controller.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.mail.alexanderkurlovich3.model.UserDTOWithProfile;

import java.util.regex.Pattern;

@Component
public class UserWithProfileValidator implements Validator{

    @Override
    public boolean supports(Class<?> aClass) {
        return UserDTOWithProfile.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "email", "user.email.empty");
        ValidationUtils.rejectIfEmpty(errors, "password", "user.password.empty");
        UserDTOWithProfile user = (UserDTOWithProfile) o;

        Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        if (!(pattern.matcher(user.getEmail()).matches())){
            errors.rejectValue("email", "user.email.invalid");
        }

        if (user.getAddress() != null) {
            if (user.getAddress().length() > 70) {
                errors.rejectValue("address", "user.address.long");
            }
        }

        if (user.getTelephone() != null) {
            if (user.getTelephone().length() > 70) {
                errors.rejectValue("telephone", "user.telephone.long");
            }
        }

        if (user.getName() != null) {
            if (user.getName().length() > 50) {
                errors.rejectValue("name", "user.name.long");
            }
        }
        if (user.getSurename() != null) {
            if (user.getSurename().length() > 50) {
                errors.rejectValue("surename", "user.surename.long");
            }
        }
    }
}
