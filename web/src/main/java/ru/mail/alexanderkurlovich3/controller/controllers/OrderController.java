package ru.mail.alexanderkurlovich3.controller.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.mail.alexanderkurlovich3.controller.properties.AddressProperties;
import ru.mail.alexanderkurlovich3.model.OrderDTO;
import ru.mail.alexanderkurlovich3.model.OrderDTOWithUser;
import ru.mail.alexanderkurlovich3.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/web/orders")
public class OrderController {

    private final OrderService orderService;
    private final AddressProperties addressProperties;

    @Autowired
    public OrderController(OrderService orderService, AddressProperties addressProperties) {
        this.orderService = orderService;
        this.addressProperties = addressProperties;
    }


    @GetMapping(value = "/current")
    @PreAuthorize("hasAuthority('CREATE_ORDER')")
    public String getOrdersForCurrentUser(ModelMap modelMap) {
        List<OrderDTO> orders = orderService.getOrdersForCurrentUser();
        modelMap.addAttribute("orders", orders);
        return addressProperties.getCustomerOrdersPagePath();
    }

    @GetMapping
    @PreAuthorize("hasAuthority('WORKING_WITH_ORDERS')")
    public String getOredersPage() {
        return "redirect:/web/orders/1";
    }


    @GetMapping(value = "/{page}")
    @PreAuthorize("hasAuthority('WORKING_WITH_ORDERS')")
    public String getOredersPage(@PathVariable("page") int page, ModelMap modelMap) {
        List<Integer> pages = orderService.getPages(page);
        modelMap.addAttribute("pages", pages);
        List<OrderDTOWithUser> orders = orderService.getUndeliveredOrdersOnPage(page);
        modelMap.addAttribute("orders", orders);
        List<String> statuses = orderService.getStatses();
        modelMap.addAttribute("statuses", statuses);
        return addressProperties.getOrdersPagePath();
    }

    @GetMapping(value = "/{orderId}/status")
    @PreAuthorize("hasAuthority('WORKING_WITH_ORDERS')")
    public String changeOrderStatus(@PathVariable("orderId") Long orderId, HttpServletRequest request) {
        String newStatus = request.getParameter("status");
        orderService.changeOrderStatus(orderId, newStatus);
        return "redirect:/web/orders/1";
    }
}
