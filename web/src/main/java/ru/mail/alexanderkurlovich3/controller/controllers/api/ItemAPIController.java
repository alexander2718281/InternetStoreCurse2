package ru.mail.alexanderkurlovich3.controller.controllers.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.mail.alexanderkurlovich3.model.ItemDTO;
import ru.mail.alexanderkurlovich3.service.ItemService;

@RestController
@RequestMapping("/api/items")
public class ItemAPIController {

    private final ItemService itemService;

    @Autowired
    public ItemAPIController(ItemService itemService) {
        this.itemService = itemService;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('API')")
    public ItemDTO createItem(@RequestBody ItemDTO item){
        return itemService.createItemFromAPI(item);
    }

    @GetMapping(value = "/{itemId}")
    @PreAuthorize("hasAuthority('API')")
    public ItemDTO createItem(@PathVariable("itemId") Long itemId){
        return itemService.getItemById(itemId);
    }

    @PutMapping(value = "/update")
    @PreAuthorize("hasAuthority('API')")
    public ItemDTO updateItem(@RequestBody ItemDTO item){
        return itemService.updateProductWithAPI(item);
    }

    @DeleteMapping(value = "/{itemId}")
    @PreAuthorize("hasAuthority('API')")
    public ItemDTO deleteItem(@PathVariable("itemId") Long itemId){
        return itemService.deleteByIdWithAPI(itemId);
    }
}
