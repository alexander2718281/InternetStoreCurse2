package ru.mail.alexanderkurlovich3.controller.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.mail.alexanderkurlovich3.controller.properties.AddressProperties;

@Controller
@RequestMapping("/login")
public class LoginController {

    private final AddressProperties addressProperties;

    @Autowired
    public LoginController(AddressProperties addressProperties) {
        this.addressProperties = addressProperties;
    }

    @GetMapping
    public String getStarPage() {
        return addressProperties.getWelcomPagePath();
    }

}
