package ru.mail.alexanderkurlovich3.controller.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.mail.alexanderkurlovich3.controller.properties.AddressProperties;
import ru.mail.alexanderkurlovich3.model.PermissiomEnum;

@Controller
@RequestMapping("/web/page")
public class PageController {

    private final AddressProperties addressProperties;

    @Autowired
    public PageController(AddressProperties addressProperties) {
        this.addressProperties = addressProperties;
    }

    @GetMapping("/security")
    @PreAuthorize("hasAuthority('SECURITY_PAGE')")
    public String toSecurityPage(ModelMap modelMap){
        return addressProperties.getSecurityPagePath();
    }

    @GetMapping("/seller")
    @PreAuthorize("hasAuthority('SELLER_PAGE')")
    public String toSellerPage(ModelMap modelMap){
        return addressProperties.getSellerPagePath();
    }

    @GetMapping("/customer")
    @PreAuthorize("hasAuthority('CUSTOMER_PAGE')")
    public String toCustomerPage(ModelMap modelMap){
        return addressProperties.getCustomerPagePath();
    }


    @GetMapping("/api")
    @PreAuthorize("hasAuthority('API')")
    public String toAPIPage(ModelMap modelMap){
        return addressProperties.getApiPagePath();
    }


}
