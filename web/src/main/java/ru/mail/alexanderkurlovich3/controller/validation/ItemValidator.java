package ru.mail.alexanderkurlovich3.controller.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.mail.alexanderkurlovich3.model.ItemDTO;

@Component
public class ItemValidator implements Validator{

    @Override
    public boolean supports(Class<?> clazz) {
        return ItemDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "uniqueNumber", "item.number.empty");
        ValidationUtils.rejectIfEmpty(errors, "price", "item.price.empty");

        ItemDTO item = (ItemDTO) target;

        if (item.getDescription() != null) {
            if (item.getDescription().length() > 255) {
                errors.rejectValue("description", "item.description.long");
            }
        }

        if (item.getName() != null) {
            if (item.getName().length() > 40) {
                errors.rejectValue("name", "item.name.long");
            }
        }

        if (item.getUniqueNumber() != null) {
            if (item.getUniqueNumber().length() > 255) {
                errors.rejectValue("uniqueNumber", "item.number.long");
            }
        }
    }
}
