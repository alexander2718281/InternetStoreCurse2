package ru.mail.alexanderkurlovich3.controller.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.mail.alexanderkurlovich3.model.CommentDTO;

@Component
public class CommentValidator implements Validator{

    @Override
    public boolean supports(Class<?> clazz) {
        return CommentDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "content", "comment.content.empty");

        CommentDTO comment = (CommentDTO) target;
        if (comment.getContent().length() > 255){
            errors.rejectValue("content", "comment.content.long");
        }
    }
}
