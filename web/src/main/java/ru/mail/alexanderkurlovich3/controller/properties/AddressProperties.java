package ru.mail.alexanderkurlovich3.controller.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AddressProperties {
    private final Environment environment;

    @Autowired
    public AddressProperties(Environment environment) {
        this.environment = environment;
    }

    private String welcomPagePath;
    private String userRegistrationPagePath;
    private String pageWithUsersPath;
    private String inputXMLPagePath;
    private String errorPagePath;
    private String securityPagePath;
    private String passwordChangePagePass;
    private String auditPagePath;
    private String sellerPagePath;
    private String newsPagePath;
    private String creteNewsPagePath;
    private String customerPagePath;
    private String choosenPagePath;
    private String updateNewsPagePath;
    private String businessCardsOfUserPagePath;
    private String createNewBusinessCardPagePath;
    private String createCommentPagePath;
    private String profilePagePath;
    private String itemsPagePath;
    private String itemCopyPagePath;
    private String cooseDiscountPagePath;
    private String customerOrdersPagePath;
    private String ordersPagePath;
    private String apiPagePath;
    private String createItemPagePath;

    @PostConstruct
    public void initialize() {
        this.welcomPagePath = environment.getProperty("welcom.page.path");
        this.userRegistrationPagePath = environment.getProperty("userregistration.page.path");
        this.pageWithUsersPath = environment.getProperty("page.with.user.path");
        this.inputXMLPagePath = environment.getProperty("input.xml.page.path");
        this.errorPagePath = environment.getProperty("error.page.path");
        this.securityPagePath = environment.getProperty("security.page");
        this.passwordChangePagePass = environment.getProperty("password.change.page.path");
        this.auditPagePath = environment.getProperty("audit.page.path");
        this.sellerPagePath = environment.getProperty("seller.page.path");
        this.newsPagePath = environment.getProperty("news.page.path");
        this.creteNewsPagePath = environment.getProperty("create.news.page.path");
        this.customerPagePath = environment.getProperty("customer.page.properties");
        this.choosenPagePath = environment.getProperty("chooseen.news.page.path");
        this.updateNewsPagePath = environment.getProperty("update.news.page.path");
        this.businessCardsOfUserPagePath = environment.getProperty("businesscards.user.page.path");
        this.createNewBusinessCardPagePath = environment.getProperty("create.business.card");
        this.createCommentPagePath = environment.getProperty("create.comment.page");
        this.profilePagePath = environment.getProperty("profile.page.path");
        this.itemsPagePath = environment.getProperty("items.page.path");
        this.itemCopyPagePath = environment.getProperty("items.copy.page.path");
        this.cooseDiscountPagePath = environment.getProperty("items.choose.discount.page.path");
        this.customerOrdersPagePath = environment.getProperty("customer.orders.page.path");
        this.ordersPagePath = environment.getProperty("orders.page.path");
        this.apiPagePath = environment.getProperty("api.page.path");
        this.createItemPagePath = environment.getProperty("create.item.page.path");
    }


    public String getAuditPagePath() {
        return auditPagePath;
    }

    public String getSellerPagePath() {
        return sellerPagePath;
    }

    public String getWelcomPagePath() {
        return welcomPagePath;
    }

    public String getUserRegistrationPagePath() {
        return userRegistrationPagePath;
    }

    public String getPageWithUsersPath() {
        return pageWithUsersPath;
    }

    public String getInputXMLPagePath() {
        return inputXMLPagePath;
    }

    public String getErrorPagePath() {
        return errorPagePath;
    }

    public String getSecurityPagePath() {
        return securityPagePath;
    }

    public String getPasswordChangePagePass() {
        return passwordChangePagePass;
    }

    public String getNewsPagePath() {
        return newsPagePath;
    }

    public String getCreteNewsPagePath() {
        return creteNewsPagePath;
    }

    public String getCustomerPagePath() {
        return customerPagePath;
    }

    public String getChoosenPagePath() {
        return choosenPagePath;
    }

    public String getUpdateNewsPagePath() {
        return updateNewsPagePath;
    }

    public String getBusinessCardsOfUserPagePath() {
        return businessCardsOfUserPagePath;
    }

    public String getCreateNewBusinessCardPagePath() {
        return createNewBusinessCardPagePath;
    }

    public String getCreateCommentPagePath() {
        return createCommentPagePath;
    }

    public String getProfilePagePath() {
        return profilePagePath;
    }

    public String getItemsPagePath() {
        return itemsPagePath;
    }

    public String getItemCopyPagePath() {
        return itemCopyPagePath;
    }

    public String getCooseDiscountPagePath() {
        return cooseDiscountPagePath;
    }

    public String getCustomerOrdersPagePath() {
        return customerOrdersPagePath;
    }

    public String getOrdersPagePath() {
        return ordersPagePath;
    }

    public String getApiPagePath() {
        return apiPagePath;
    }

    public String getCreateItemPagePath() {
        return createItemPagePath;
    }
}
