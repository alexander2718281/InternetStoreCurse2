package ru.mail.alexanderkurlovich3.controller.validation;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.mail.alexanderkurlovich3.model.BusinessCardDTO;

@Component
public class BusinessCardValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return BusinessCardDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "title", "cards.title.empty");

        BusinessCardDTO card = (BusinessCardDTO) o;

        if (card.getWorkingTelephone().length() > 20){
            errors.rejectValue("workingTelephone", "cards.telephone.long");
        }

    }
}
