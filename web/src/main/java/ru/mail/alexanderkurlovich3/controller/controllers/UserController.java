package ru.mail.alexanderkurlovich3.controller.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.mail.alexanderkurlovich3.controller.properties.AddressProperties;
import ru.mail.alexanderkurlovich3.controller.validation.ProfileUpdateValidator;
import ru.mail.alexanderkurlovich3.controller.validation.UserWithProfileValidator;
import ru.mail.alexanderkurlovich3.model.RoleDTO;
import ru.mail.alexanderkurlovich3.model.UserDTO;
import ru.mail.alexanderkurlovich3.model.UserDTOWithProfile;
import ru.mail.alexanderkurlovich3.service.RoleService;
import ru.mail.alexanderkurlovich3.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/web/users")
public class UserController {

    private final AddressProperties addressProperties;
    private final UserService userService;
    private final RoleService roleService;
    private final UserWithProfileValidator userValidetor;
    private final ProfileUpdateValidator profileUpdateValidator;

    @Autowired
    public UserController(AddressProperties addressProperties, UserService userService,
                          RoleService roleService, UserWithProfileValidator userValidetor,
                          ProfileUpdateValidator profileUpdateValidator) {
        this.addressProperties = addressProperties;
        this.userService = userService;
        this.roleService = roleService;
        this.userValidetor = userValidetor;
        this.profileUpdateValidator = profileUpdateValidator;
    }

    @GetMapping("/create")
    public String getRegistrationPage(ModelMap modelMap) {
        modelMap.addAttribute("user", new UserDTOWithProfile());
        return addressProperties.getUserRegistrationPagePath();
    }

    @GetMapping
    @PreAuthorize("hasAuthority('USERS_CONTROL')")
    public String getUsers() {
        return "redirect:/web/users/1";
    }

    @PostMapping(value = "/create")
    public String createUser(
            @ModelAttribute("user") UserDTOWithProfile user,
            BindingResult result,
            ModelMap modelMap
    ) {
        userValidetor.validate(user, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("user", user);
            return addressProperties.getUserRegistrationPagePath();
        } else {
            int res = userService.register(user);
            if (res == 0) {
                modelMap.addAttribute("user", user);
                modelMap.addAttribute("massage", "This email already exist");
                return addressProperties.getUserRegistrationPagePath();
            }
            return addressProperties.getWelcomPagePath();
        }
    }

    @GetMapping(value = "/{page}")
    @PreAuthorize("hasAuthority('USERS_CONTROL')")
    public String getUsers(@PathVariable("page") int page, ModelMap modelMap) {
        List<Integer> pages = userService.getPages(page);
        modelMap.addAttribute("pages", pages);
        List<UserDTOWithProfile> users = userService.getUsersInPage(page);
        modelMap.addAttribute("users", users);
        List<RoleDTO> roles = roleService.getAllRoles();
        modelMap.addAttribute("roles", roles);
        return addressProperties.getPageWithUsersPath();
    }

    @PostMapping(value = "/able")
    @PreAuthorize("hasAuthority('USERS_CONTROL')")
    public String changeAbility(@RequestParam("ids") Long[] ids) {
        for (Long id : ids) {
            userService.changeAbility(id);
        }
        return "redirect:/web/users";
    }

    @GetMapping(value = "/role/{id}")
    @PreAuthorize("hasAuthority('USERS_CONTROL')")
    public String changeRole(@PathVariable("id") Long userId, HttpServletRequest request) {
        Long roleId = Long.valueOf(request.getParameter("role"));
        userService.changeRole(userId, roleId);
        return "redirect:/web/users";
    }

    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('USERS_CONTROL')")
    public String deleteUsers(@PathVariable("id") Long id) {
        userService.deleteUser(id);
        return "redirect:/web/users";
    }

    @GetMapping(value = "/password/{id}")
    @PreAuthorize("hasAuthority('USERS_CONTROL')")
    public String passwordChangePage(@PathVariable("id") Long userId, ModelMap modelMap) {
        UserDTO user = userService.getUserById(userId);
        modelMap.addAttribute("user", user);
        return addressProperties.getPasswordChangePagePass();
    }

    @PostMapping(value = "/password/{id}")
    @PreAuthorize("hasAuthority('USERS_CONTROL')")
    public String changePassword(@PathVariable("id") Long userId, HttpServletRequest request) {
        String newPassword = request.getParameter("password");
        userService.changePassword(userId, newPassword);
        return "redirect:/web/users";
    }

    @GetMapping(value = "/profile")
    @PreAuthorize("hasAuthority('REDACT_PROFILE')")
    public String getProfilePage(ModelMap modelMap) {
        UserDTOWithProfile user = userService.getAuthoriseUser();
        if (user == null) {
            return "redirect:/login";
        }
        modelMap.addAttribute("user", user);
        return addressProperties.getProfilePagePath();
    }

    @PostMapping(value = "/profile")
    @PreAuthorize("hasAuthority('REDACT_PROFILE')")
    public String updateUser(
            @ModelAttribute("user") UserDTOWithProfile user,
            BindingResult result,
            ModelMap modelMap) {
        profileUpdateValidator.validate(user, result);
        if (result.hasErrors()) {
            modelMap.addAttribute("user", user);
            return addressProperties.getProfilePagePath();
        } else {
            String role = userService.updateUser(user);
            switch (role) {
                case "SECURITY":
                    return addressProperties.getSecurityPagePath();
                case "SELLER":
                    return addressProperties.getSellerPagePath();
                case "CUSTOMER":
                    return addressProperties.getCustomerPagePath();
                default:
                    return "redirect:/login";
            }
        }
    }
}
