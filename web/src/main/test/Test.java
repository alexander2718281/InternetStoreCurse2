import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.mail.alexanderkurlovich3.dao.UserDao;
import ru.mail.alexanderkurlovich3.dao.impl.UserDaoImpl;
import ru.mail.alexanderkurlovich3.dao.model.User;
import ru.mail.alexanderkurlovich3.model.*;
import ru.mail.alexanderkurlovich3.service.*;
import ru.mail.alexanderkurlovich3.service.impl.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;


public class Test {

    private static Random rnd = new Random();
    private static final Logger logger = LogManager.getLogger(Test.class);
/*    private ItemService productService = new ItemServiceImpl();
    private DiscountService discountService = new DiscountServiceImpl();
    private OrderService orderService = new OrderServiceImpl();
    private RoleService roleService = new RoleServiceImpl();*/

    @org.junit.Test
    public void main() {
        UserDao userDao = new UserDaoImpl();
        List<User> users = userDao.getUsersInPage(1,2);
        for (User user : users) {
            logger.info(user);
        }
 /*       roleService.createRole("USER");

         List<ItemDTO> productDTOS = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            ItemDTO productDTO = new ItemDTO();
            productDTO.setPrice(BigDecimal.valueOf(getRandomInDiapasone(100, 500)));
            productDTO.setUniqueNumber(Integer.valueOf(i).toString());
            productDTO.setName("Item" + i);
            productDTO.setDescription("Description" + i);
            productDTOS.add(productDTO);
        }
        productService.saveProducts(productDTOS);

        for (int i = 0; i < 3; i++) {
            DiscountDTO discountDTO = new DiscountDTO();
            discountDTO.setPercent(10*(i+1));
            discountDTO.setName("Discount " + (i+1));
            LocalDate finalDate = LocalDate.of(2019, 1, 1);
            Instant instant = finalDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
            Date date = Date.from(instant);
            discountDTO.setFinalDate(date);
            discountService.save(discountDTO);
        }

        List<DiscountDTO> discountDTOS = discountService.getActualDiscount();
        for (DiscountDTO discountDTO : discountDTOS) {
            List<ItemDTO> productDTOS1 = new ArrayList<>();
            switch (discountDTO.getPercent()){
                case 30:
                    productDTOS1 = productService.getProductsInPriceRange(400, 500);
                    break;
                case 20:
                    productDTOS1 = productService.getProductsInPriceRange(300, 399);
                    break;
                case 10:
                    productDTOS1 = productService.getProductsInPriceRange(200, 299);
                    break;
            }
            for (ItemDTO productDTO : productDTOS1) {
                discountService.addDiscountToProduct(productDTO.getId(), discountDTO.getId());
            }
        }

        List<ItemDTO> productDTOS2 = productService.getProductsByDiscount(0);
        for (ItemDTO productDTO : productDTOS2) {
            logger.info(productDTO);
        }

        UserDTOWithProfile userDTOWithProfile = new UserDTOWithProfile();
        userDTOWithProfile.setType("USER");
        userDTOWithProfile.setAddress("address");
        userDTOWithProfile.setTelephone("34");
        userDTOWithProfile.setPassword("password");
        userDTOWithProfile.setEmail("user@user");
        userDTOWithProfile.setName("user");
        userDTOWithProfile.setSurename("userov");

        UserService userService = new UserServiceImpl();
        userService.register(userDTOWithProfile);

        UserDTOForLogin userDTOForLogin = userService.login("user@user","password");

        discountService.addDiscountToUser(userDTOForLogin.getId(), 1);

        List<ItemDTO> productDTOS1 = productService.getProductsInPriceRange(250, 450);
        long count = productService.countOfProductsInRange(250, 450);
        for (int i = 0; i < 4; i++) {
            ItemDTO productDTO = productDTOS1.get(getRandomInDiapasone(0, productDTOS1.size()-1));
            orderService.save(productDTO.getId(), userDTOForLogin.getId(), (int) count);
        }

        List<OrderDTOWithUser> orderDTOS = orderService.getUndeliveredOrders();
        for (OrderDTOWithUser orderDTO : orderDTOS) {
            logger.info(orderDTO);
        }*/

    }

    private static int getRandomInDiapasone(int min, int max){
        return rnd.nextInt(max - min + 1) + min;
    }

}