<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Create business card</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br/>
<br/>
<h2 align="center">Create you own new business card</h2>
<br/>
<form:form action="${pageContext.request.contextPath}/web/cards/create" modelAttribute="card" method="post">
    <form:errors path="*" cssClass="errorblock" element="div"/>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="title" class="col-sm-1 col-form-label">Title</form:label>
        <div class="col-sm-3">
            <form:input path="title" type="text" class="form-control" placeholder="Title"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="fullName" class="col-sm-1 col-form-label">Full Name:</form:label>
        <div class="col-sm-3">
            <form:input path="fullName" type="text" class="form-control" placeholder="Write full name"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="workingTelephone" class="col-sm-1 col-form-label">Working Telephone:</form:label>
        <div class="col-sm-3">
            <form:input path="workingTelephone" type="text" class="form-control" placeholder="Write your working telephone"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
        <div class="col-sm-4"></div>
    </div>
</form:form>

<jsp:include page="util/js.jsp"/>
</body>
</html>

