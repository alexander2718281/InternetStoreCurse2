<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Discounts</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br>
<h1 align="center">Choose discount</h1>
<table class="table">
    <thead class="thead-light">
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Percent</th>
        <th scope="col">Final date</th>
        <th scope="col">Choose</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${discounts}" var="discount">
        <tr>
            <th scope="row">${discount.name}</th>
            <td>${discount.percent}</td>
            <td>${discount.finalDate}</td>
            <td><a href="${pageContext.request.contextPath}/web/items/${itemId}/discount?discountId=${discount.id}"
                   class="col-sm-3 btn btn-primary" role="button" aria-pressed="true">Add</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<jsp:include page="util/js.jsp"/>

</body>
</html>
