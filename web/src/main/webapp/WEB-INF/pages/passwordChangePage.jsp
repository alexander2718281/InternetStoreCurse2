<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Password change</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br>
<h2 align="center">Change password</h2>
<br>
<form action="${pageContext.request.contextPath}/web/users/password/${user.id}" method="post">
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <label for="exampleInputEmail1" class="col-sm-1 col-form-label">Enter new password</label>
        <input type="password" class="col-sm-3 form-control" name="password" id="exampleInputEmail1"
               aria-describedby="emailHelp" placeholder="New password">
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <button type="submit" class="col-sm-1 btn btn-primary">Change</button>
        <div class="col-sm-7"></div>
    </div>
</form>
<jsp:include page="util/js.jsp"/>
</body>
</html>
