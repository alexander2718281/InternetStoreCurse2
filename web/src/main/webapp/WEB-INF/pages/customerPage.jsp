<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Customer Page</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br/>
<br/>
<h2 align="center">Customer Page</h2>
<br/>
<a href="${pageContext.request.contextPath}/web/news" type="button"
   class="btn btn-warning btn-lg btn-block">Show news</a>
<a href="${pageContext.request.contextPath}/web/items" type="button"
   class="btn btn-warning btn-lg btn-block">Go to shop</a>
<a href="${pageContext.request.contextPath}/web/orders/current" type="button"
   class="btn btn-warning btn-lg btn-block">Show your orders</a>
<a href="${pageContext.request.contextPath}/web/cards" type="button"
   class="btn btn-warning btn-lg btn-block">Work with your business cards</a>
<jsp:include page="util/js.jsp"/>
</body>
</html>
