<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Your orders</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br>
<h1 align="center">Your orders</h1>
<table class="table">
    <thead class="thead-light">
    <tr>
        <th scope="col">Item name</th>
        <th scope="col">Quantity</th>
        <th scope="col">Date</th>
        <th scope="col">Summ</th>
        <th scope="col">Summ with discount</th>
        <th scope="col">Status</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${orders}" var="order">
        <tr>
            <th scope="row">${order.itemName}</th>
            <td>${order.quantity}</td>
            <td>${order.created}</td>
            <td>${order.summ}</td>
            <td>${order.summWithDiscount}</td>
            <td>${order.status}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<jsp:include page="util/js.jsp"/>

</body>
</html>
