<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Security Page</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br/>
<br/>
<h2 align="center">Security Page</h2>
<br/>
<a href="${pageContext.request.contextPath}/web/users" type="button"
   class="btn btn-warning btn-lg btn-block">Показать список пользователей</a>
<a href="${pageContext.request.contextPath}/web/audit" type="button"
   class="btn btn-warning btn-lg btn-block">Показать список совершённых действий</a>


<jsp:include page="util/js.jsp"/>
</body>
</html>
