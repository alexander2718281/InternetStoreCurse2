<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Update items page</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form method="post" action="${pageContext.request.contextPath}/web/items/update"
                  enctype="multipart/form-data">
                <table>
                    <tr>
                        <td><label for="file">Select a file to updating</label></td>
                        <td><input id="file" type="file" name="file"/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>


<jsp:include page="util/js.jsp"/>
</body>
</html>
