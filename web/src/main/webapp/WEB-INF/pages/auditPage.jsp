<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Users</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br>
<h1 align="center">Last events</h1>
<table class="table">
    <thead class="thead-light">
    <tr>
        <th scope="col">Event type</th>
        <th scope="col">Event</th>
        <th scope="col">Date</th>
        <th scope="col">User</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${audits}" var="audit">
        <tr>
            <th scope="row">${audit.type}</th>
            <td>${audit.event}</td>
            <td>${audit.created}</td>
            <td>${audit.userEmail}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<nav aria-label="Pages with users">
    <ul class="pagination justify-content-end">
        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/web/audit/1">First page</a>
        </li>
        <c:forEach items="${pages}" var="page">
            <li class="page-item"><a class="page-link"
                                     href="${pageContext.request.contextPath}/web/audit/${page}"><c:out
                    value="${page}"/></a></li>
        </c:forEach>
        <li class="page-item"><a class="page-link" href="#">Last page</a></li>
    </ul>
</nav>
<jsp:include page="util/js.jsp"/>

</body>
</html>
