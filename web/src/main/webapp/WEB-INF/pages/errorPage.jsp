<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Error</title>
</head>
<body>
<br/>
<h1 align="center">Application Error</h1>
<h3>Debug information:</h3>

Request URL = ${url}
<br/>

Exception = ${exception.message}
<br/>
<strong>Exception stack trace:</strong>
<c:forEach items="${exception.stackTrace}" var="ste">
    ${ste}
</c:forEach>
<jsp:include page="util/js.jsp"/>
</body>
</html>
