<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Your business cards</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br>
<h1 align="center">Your business cards</h1>
<table class="table">
    <thead class="thead-light">
    <tr>
        <th scope="col">Title</th>
        <th scope="col">Full name</th>
        <th scope="col">Working telephone</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${cards}" var="card">
        <tr>
            <th scope="row">${card.title}</th>
            <td>${card.fullName}</td>
            <td>${card.workingTelephone}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="col-md-2">
        <a href="${pageContext.request.contextPath}/web/cards/create"
           class="col-sm-6 btn btn-primary" role="button" aria-pressed="true">Create new card</a>
</div>
<jsp:include page="util/js.jsp"/>

</body>
</html>