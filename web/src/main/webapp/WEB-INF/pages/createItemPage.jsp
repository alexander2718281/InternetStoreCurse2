<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Create new Item</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br/>
<br/>
<h2 align="center">Create new item</h2>
<br/>
<form:form action="${pageContext.request.contextPath}/web/items/create" modelAttribute="item" method="post">
    <form:errors path="*" cssClass="errorblock" element="div"/>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="uniqueNumber" class="col-sm-1 col-form-label">Unique Number</form:label>
        <div class="col-sm-3">
            <form:input path="uniqueNumber" type="text" class="form-control" placeholder="Write unique number"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="name" class="col-sm-1 col-form-label">Name:</form:label>
        <div class="col-sm-3">
            <form:input path="name" type="text" class="form-control" placeholder="Write name"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="description" class="col-sm-1 col-form-label">Description:</form:label>
        <div class="col-sm-3">
            <form:input path="description" type="text" class="form-control" placeholder="Write description"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="price" class="col-sm-1 col-form-label">Price:</form:label>
        <div class="col-sm-3">
            <form:input path="price" type="text" class="form-control" placeholder="Write price for item"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
        <div class="col-sm-4"></div>
    </div>
</form:form>
<c:if test="${massage != null}">
    <div class="alert alert-danger" role="alert">
        <h4 align="center"><c:out value="${massage}"/></h4>
    </div>
</c:if>

<jsp:include page="util/js.jsp"/>
</body>
</html>

