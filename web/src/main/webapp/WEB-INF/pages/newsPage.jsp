<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>News</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br/>
<h2 align="center">News</h2>
<table class="table">
    <thead class="thead-light">
    <tr>
        <th scope="col">Title</th>
        <th scope="col">Content</th>
        <th scope="col">Date</th>
        <th scope="col">Author</th>
        <th scope="col">Read</th>
        <security:authorize access="hasAuthority('CUD_NEWS')">
            <th scope="col">Delete</th>
        </security:authorize>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${news}" var="point">
        <tr>
            <th scope="row">${point.title}</th>
            <td>${point.content}</td>
            <td>${point.created}</td>
            <td>${point.userEmail}</td>
            <td>
                <a href="${pageContext.request.contextPath}/web/news/read/${point.id}?page=1"
                   class="col-sm-6 btn btn-primary" role="button" aria-pressed="true">Read</a>
            </td>
            <security:authorize access="hasAuthority('CUD_NEWS')">
                <td><a href="${pageContext.request.contextPath}/web/news/delete/${point.id}" class="badge badge-danger">X</a></td>
            </security:authorize>
        </tr>
    </c:forEach>
    </tbody>
</table>
<nav aria-label="Pages with users">
    <ul class="pagination justify-content-end">
        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/web/news/1">First
            page</a>
        </li>
        <c:forEach items="${pages}" var="page">
            <li class="page-item"><a class="page-link"
                                     href="${pageContext.request.contextPath}/web/news/${page}"><c:out
                    value="${page}"/></a></li>
        </c:forEach>
    </ul>
</nav>
<div class="col-md-2">
    <security:authorize access="hasAuthority('CUD_NEWS')">
        <a href="${pageContext.request.contextPath}/web/news/create"
           class="col-sm-6 btn btn-primary" role="button" aria-pressed="true">Create news</a>
    </security:authorize>
</div>

<jsp:include page="util/js.jsp"/>

</body>
</html>
