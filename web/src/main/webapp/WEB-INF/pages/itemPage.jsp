<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>News</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br/>
<h2 align="center">Shop</h2>
<table class="table">
    <thead class="thead-light">
    <tr>
        <th scope="col">Number</th>
        <th scope="col">Name</th>
        <th scope="col">Description</th>
        <th scope="col">Price</th>
        <th scope="col">Price with discount</th>
        <security:authorize access="hasAuthority('CUD_ITEMS')">
            <th scope="col">Copy</th>
            <th scope="col">Add discount</th>
            <th scope="col">Delete</th>
        </security:authorize>
        <security:authorize access="hasAuthority('CREATE_ORDER')">
            <th scope="col">Quantity</th>
            <th scope="col">Buy</th>
        </security:authorize>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${items}" var="item">
        <tr>
            <th scope="row">${item.uniqueNumber}</th>
            <td>${item.name}</td>
            <td>${item.description}</td>
            <td>${item.price}</td>
            <td>${item.priceWithDiscount}</td>
            <security:authorize access="hasAuthority('CUD_ITEMS')">
                <td>
                    <a href="${pageContext.request.contextPath}/web/items/${item.id}/copy"
                       class="col-sm-5 btn btn-primary" role="button" aria-pressed="true">Copy</a>
                </td>
                <td>
                    <a href="${pageContext.request.contextPath}/web/items/${item.id}/discounts"
                       class="col-sm-6 btn btn-primary" role="button" aria-pressed="true">Add discount</a>
                </td>
                <td><a href="${pageContext.request.contextPath}/web/items/${item.id}/delete"
                       class="badge badge-danger">X</a></td>
            </security:authorize>
            <security:authorize access="hasAuthority('CREATE_ORDER')">
                <form name="num" method="post" action="${pageContext.request.contextPath}/web/items/${item.id}/order">
                    <td><input type="number" name="num" placeholder="Quantity"/></td>
                    <td><input type="submit" class="col-sm-4 btn btn-primary" value="Buy"/></td>
                </form>
            </security:authorize>
        </tr>
    </c:forEach>
    </tbody>
</table>
<nav aria-label="Pages with items">
    <ul class="pagination justify-content-end">
        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/web/items/1">First
            page</a>
        </li>
        <c:forEach items="${pages}" var="page">
            <li class="page-item"><a class="page-link"
                                     href="${pageContext.request.contextPath}/web/items/${page}"><c:out
                    value="${page}"/></a></li>
        </c:forEach>
    </ul>
</nav>
<security:authorize access="hasAuthority('CUD_ITEMS')">
    <div class="row">
        <div class="col-md-2">
            <a href="${pageContext.request.contextPath}/web/items/create"
               class="col-sm-6 btn btn-primary" role="button" aria-pressed="true">Create new item</a>
        </div>
        <div class="col-sm-1"></div>
        <div class="col-md-3">
            <a href="${pageContext.request.contextPath}/web/items/update"
               class="col-sm-6 btn btn-primary" role="button" aria-pressed="true">Update items with xml</a>
        </div>
    </div>
</security:authorize>

<jsp:include page="util/js.jsp"/>

</body>
</html>
