<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Copy item</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br>
<h2 align="center">Unique number for copied item</h2>
<br>
<form action="${pageContext.request.contextPath}/web/items/${itemId}/copy" method="post">
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <label for="newUniqueNumber" class="col-sm-2 col-form-label">Enter new unique number for copied item</label>
        <input type="text" class="col-sm-2 form-control" name="number" id="newUniqueNumber"
               aria-describedby="emailHelp" placeholder="New unique number">
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <button type="submit" class="col-sm-1 btn btn-primary">Copy</button>
        <div class="col-sm-7"></div>
    </div>
</form>
<jsp:include page="util/js.jsp"/>
</body>
</html>
