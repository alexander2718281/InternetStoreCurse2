<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Seller Page</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br/>
<br/>
<h2 align="center">Seller Page</h2>
<br/>
<a href="${pageContext.request.contextPath}/web/news" type="button"
   class="btn btn-warning btn-lg btn-block">Work with news</a>
<a href="${pageContext.request.contextPath}/web/items" type="button"
class="btn btn-warning btn-lg btn-block">Redact goods in store</a>
<a href="${pageContext.request.contextPath}/web/orders" type="button"
   class="btn btn-warning btn-lg btn-block">Work with orders</a>
<jsp:include page="util/js.jsp"/>
</body>
</html>
