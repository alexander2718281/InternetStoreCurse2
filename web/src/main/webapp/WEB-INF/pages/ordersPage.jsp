<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Orders</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<h1 align="center">Orders</h1>
<table class="table">
    <thead class="thead-light">
    <tr>
        <th scope="col">Item name</th>
        <th scope="col">User email</th>
        <th scope="col">Quantity</th>
        <th scope="col">Date</th>
        <th scope="col">Summ</th>
        <th scope="col">Summ with discount</th>
        <th scope="col">Status</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${orders}" var="order">
        <tr>
            <th scope="row">${order.itemName}</th>
            <td>${order.userEmail}</td>
            <td>${order.quantity}</td>
            <td>${order.created}</td>
            <td>${order.summ}</td>
            <td>${order.summWithDiscount}</td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <c:out value="${order.status}"/>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu">
                        <c:forEach items="${statuses}" var="status">
                            <a class="dropdown-item"
                               href="${pageContext.request.contextPath}/web/orders/${order.id}/status?status=${status}">${status}</a>
                        </c:forEach>
                    </div>
                </div>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="form-group row">
    <div class="col-sm-1"></div>
    <div class="col-sm-1">
        <button type="submit" class="btn btn-primary">Change ability</button>
    </div>
    <div class="col-sm-10"></div>
</div>
<nav aria-label="Pages with users">
    <ul class="pagination justify-content-end">
        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/web/orders/1">First page</a>
        </li>
        <c:forEach items="${pages}" var="page">
            <li class="page-item"><a class="page-link"
                                     href="${pageContext.request.contextPath}/web/orders/${page}"><c:out
                    value="${page}"/></a></li>
        </c:forEach>
        <li class="page-item"><a class="page-link" href="#">Last page</a></li>
    </ul>
</nav>
<jsp:include page="util/js.jsp"/>
</body>
</html>