<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>Users</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<h1 align="center">Users</h1>
<form action="${pageContext.request.contextPath}/web/users/able" method="post">
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Email</th>
            <th scope="col">Фамилия</th>
            <th scope="col">Имя</th>
            <th scope="col">Телефон</th>
            <th scope="col">Адресс</th>
            <th scope="col">Роль</th>
            <th scope="col">Активен</th>
            <th scope="col">Смена пароля</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${users}" var="user">
            <tr>
                <th scope="row"><input type="checkbox" name="ids" value="${user.id}"></th>
                <td>${user.email}</td>
                <td>${user.surename}</td>
                <td>${user.name}</td>
                <td>${user.telephone}</td>
                <td>${user.address}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <c:out value="${user.type}"/>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu">
                            <c:forEach items="${roles}" var="role">
                                <a class="dropdown-item"
                                   href="${pageContext.request.contextPath}/web/users/role/${user.id}?role=${role.id}">${role.name}</a>
                            </c:forEach>
                        </div>
                    </div>
                </td>
                <td>${user.enable}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/web/users/password/${user.id}"
                       class="col-sm-6 btn btn-primary" role="button" aria-pressed="true">Change password</a>
                </td>
                <td><a href="${pageContext.request.contextPath}/web/users/delete/${user.id}" class="badge badge-danger">X</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="form-group row">
        <div class="col-sm-1"></div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary">Change ability</button>
        </div>
        <div class="col-sm-10"></div>
    </div>
</form>
<nav aria-label="Pages with users">
    <ul class="pagination justify-content-end">
        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/web/users/1">First page</a>
        </li>
        <c:forEach items="${pages}" var="page">
            <li class="page-item"><a class="page-link"
                                     href="${pageContext.request.contextPath}/web/users/${page}"><c:out
                    value="${page}"/></a></li>
        </c:forEach>
        <li class="page-item"><a class="page-link" href="#">Last page</a></li>
    </ul>
</nav>
<jsp:include page="util/js.jsp"/>
</body>
</html>
