<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Create news</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br/>
<br/>
<h2 align="center">Write new news</h2>
<br/>
<form:form action="${pageContext.request.contextPath}/web/news/create" modelAttribute="news" method="post">
    <form:errors path="*" cssClass="errorblock" element="div"/>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="title" class="col-sm-1 col-form-label">Title</form:label>
        <div class="col-sm-3">
            <form:input path="title" type="text" class="form-control" placeholder="Title"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="content" class="col-sm-1 col-form-label">News:</form:label>
        <div class="col-sm-3">
            <form:input path="content" type="text" class="form-control" placeholder="Write you message"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
        <div class="col-sm-4"></div>
    </div>
</form:form>

<jsp:include page="util/js.jsp"/>
</body>
</html>
