<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Comment</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br/>
<br/>
<h2 align="center">Leave a comment</h2>
<br/>
<form:form action="${pageContext.request.contextPath}/web/news/${newsId}/comment" modelAttribute="comment" method="post">
    <form:errors path="*" cssClass="errorblock" element="div"/>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="content" class="col-sm-1 col-form-label">Your comment:</form:label>
        <div class="col-sm-3">
            <form:input path="content" type="text" class="form-control" placeholder="Write your comment here"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Comment</button>
        </div>
        <div class="col-sm-4"></div>
    </div>
</form:form>

<jsp:include page="util/js.jsp"/>
</body>
</html>

