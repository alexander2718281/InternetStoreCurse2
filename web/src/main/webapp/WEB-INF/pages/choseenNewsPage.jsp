<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <jsp:include page="util/head.jsp"/>
    <title>News</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>
<br>
<h1 align="center">News</h1>

<table class="table">
    <thead class="thead-light">
    <tr>
        <th scope="col">Title</th>
        <th scope="col">Content</th>
        <th scope="col">Date</th>
        <th scope="col">Author</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">${news.title}</th>
        <td>${news.content}</td>
        <td>${news.created}</td>
        <td>${news.userEmail}</td>
    </tbody>
</table>
<security:authorize access="hasAuthority('CUD_NEWS')">
    <div class="form-group row">
        <div class="col-sm-1"></div>
        <a href="${pageContext.request.contextPath}/web/news/update/${news.id}" class="col-sm-2 btn btn-primary"
           role="button"
           aria-pressed="true">Update News</a>
        <div class="col-sm-9"></div>
    </div>
</security:authorize>
<br/>
<h4>Comments:</h4>
</table>
<div class="form-group row">
    <div class="col-sm-1"></div>

    <table class="table col-sm-10">
        <thead class="thead-light">
        <tr>
            <th scope="col">Content</th>
            <th scope="col">Date</th>
            <th scope="col">Author</th>
            <security:authorize access="hasAuthority('CUD_NEWS')">
                <th scope="col">Delete</th>
            </security:authorize>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${comments}" var="comment">
            <tr>
                <th scope="row">${comment.content}</th>
                <td>${comment.created}</td>
                <td>${comment.userEmail}</td>
                <security:authorize access="hasAuthority('CUD_NEWS')">
                    <td>
                        <a href="${pageContext.request.contextPath}/web/news/${news.id}/comment/delete?comment=${comment.id}"
                           class="badge badge-danger">X</a></td>
                </security:authorize>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="col-sm-1"></div>
</div>
<div class="col-md-2">
    <a href="${pageContext.request.contextPath}/web/news/${news.id}/comment"
       class="col-sm-6 btn btn-primary" role="button" aria-pressed="true">Comment</a>
</div>
<nav aria-label="Comments">
    <ul class="pagination justify-content-end">
        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/web/news/read/${news.id}?page=1">First page</a>
        </li>
        <c:forEach items="${pages}" var="page">
            <li class="page-item"><a class="page-link"
                                     href="${pageContext.request.contextPath}/web/news/read/${news.id}?page=${page}"><c:out
                    value="${page}"/></a></li>
        </c:forEach>
    </ul>
</nav>
<jsp:include page="util/js.jsp"/>

</body>
</html>