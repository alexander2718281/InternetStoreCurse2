<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <jsp:include page="util/head.jsp"/>
    <title>Profile</title>
</head>
<body>
<br/>
<br/>
<h2 align="center">Your profile</h2>
<br/>
<form:form action="${pageContext.request.contextPath}/web/users/profile" modelAttribute="user" method="post">
    <div align="center"><form:errors path="*" cssClass="errorBlock" element="div"/></div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <div class="col-sm-1">Email:</div>
        <div class="col-sm-3">
            <c:out value="${user.email}"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="password" class="col-sm-1 col-form-label">Password:</form:label>
        <div class="col-sm-3">
            <form:input path="password" type="password" class="form-control" placeholder="Write your new password, if you want to"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="surename" class="col-sm-1 col-form-label">Surename:</form:label>
        <div class="col-sm-3">
            <form:input path="surename" type="text" class="form-control" placeholder="Surename"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="name" class="col-sm-1 col-form-label">Name:</form:label>
        <div class="col-sm-3">
            <form:input path="name" type="text" class="form-control" placeholder="Name"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="telephone" class="col-sm-1 col-form-label">Telephone:</form:label>
        <div class="col-sm-3">
            <form:input path="telephone" type="tel" class="form-control" placeholder="Telephone"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <form:label path="address" class="col-sm-1 col-form-label">Address:</form:label>
        <div class="col-sm-3">
            <form:input path="address" type="text" class="form-control" placeholder="Address"/>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Change information</button>
        </div>
        <div class="col-sm-4"></div>
    </div>
</form:form>

<jsp:include page="util/js.jsp"/>
</body>
</html>