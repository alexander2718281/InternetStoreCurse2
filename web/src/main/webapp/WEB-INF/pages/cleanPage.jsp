<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <jsp:include page="util/head.jsp"/>
    <title>API page</title>
</head>
<body>
<jsp:include page="util/logoutAndRedactButtons.jsp"/>

<jsp:include page="util/js.jsp"/>
</body>
</html>
