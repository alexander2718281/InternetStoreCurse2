<div class="form-group row">
    <div class="col-sm-7"></div>
    <a href="${pageContext.request.contextPath}/web/users/profile" class="col-sm-1 btn btn-primary" role="button"
       aria-pressed="true">Redact profile</a>
    <div class="col-sm-1"></div>
    <a href="${pageContext.request.contextPath}/logout" class="col-sm-1 btn btn-primary" role="button"
       aria-pressed="true">Logout</a>
    <div class="col-sm-1"></div>
</div>
