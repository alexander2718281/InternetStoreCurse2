package ru.mail.alexanderkurlovich3.service.util;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.helpers.DefaultHandler;
import ru.mail.alexanderkurlovich3.model.ItemDTO;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@Component
public class SAXParserUtil {

    private static final Logger logger = LogManager.getLogger(SAXParserUtil.class);


    private final DefaultHandler productHandler;

    @Autowired
    public SAXParserUtil(DefaultHandler productHandler) {
        this.productHandler = productHandler;
    }

    public List<ItemDTO> saxParser(String address) {
        List<ItemDTO> products = null;
        try {
            File inputFile = new File(address);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(inputFile, productHandler);
            ProductHandler citedHandler = (ProductHandler) productHandler;
            products = citedHandler.getProducts();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return products;
    }

    public List<ItemDTO> saxParser(MultipartFile file) {
        List<ItemDTO> products = null;
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(convert(file), productHandler);
            ProductHandler citedHandler = (ProductHandler) productHandler;
            products = citedHandler.getProducts();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return products;
    }

    private File convert(MultipartFile file) {
        File convFile = new File(file.getOriginalFilename());
        try {
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return convFile;
    }
}
