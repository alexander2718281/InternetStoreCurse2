package ru.mail.alexanderkurlovich3.service.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PagesProperties {

    private final Environment environment;

    private int countOfFieldsInPage;
    private int countOfUsersInPage;
    private int countOfAuditsInPage;
    private int countOfNewsOnPage;
    private int countOfCommentsOnPage;
    private int countOfItemsOnPage;
    private int countOfOrdersOnPage;

    @Autowired
    public PagesProperties(Environment environment) {
        this.environment = environment;
    }

    @PostConstruct
    public void initialize() {
        this.countOfFieldsInPage = Integer.parseInt(environment.getProperty("fields.on.page.default"));
        this.countOfUsersInPage = Integer.parseInt(environment.getProperty("users.on.page"));
        this.countOfAuditsInPage = Integer.parseInt(environment.getProperty("audits.on.page"));
        this.countOfNewsOnPage = Integer.valueOf(environment.getProperty("news.on.page"));
        this.countOfCommentsOnPage = Integer.valueOf(environment.getProperty("comments.on.page"));
        this.countOfItemsOnPage = Integer.valueOf(environment.getProperty("items.on.page"));
        this.countOfOrdersOnPage = Integer.valueOf(environment.getProperty("orders.on.page"));
    }

    public int getCountOfFieldsInPage() {
        return countOfFieldsInPage;
    }

    public int getCountOfUsersInPage() {
        return countOfUsersInPage;
    }

    public int getCountOfAuditsInPage() {
        return countOfAuditsInPage;
    }

    public int getCountOfNewsOnPage() {
        return countOfNewsOnPage;
    }

    public int getCountOfCommentsOnPage() {
        return countOfCommentsOnPage;
    }

    public int getCountOfItemsOnPage() {
        return countOfItemsOnPage;
    }

    public int getCountOfOrdersOnPage() {
        return countOfOrdersOnPage;
    }
}
