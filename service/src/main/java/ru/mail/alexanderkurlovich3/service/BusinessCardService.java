package ru.mail.alexanderkurlovich3.service;


import ru.mail.alexanderkurlovich3.model.BusinessCardDTO;

import java.util.List;

public interface BusinessCardService {

    List<BusinessCardDTO> getCardsForUser ();

    List<BusinessCardDTO> getCardsForUser (Long userId);

    void createNewBusinessCardForUser(BusinessCardDTO cardDTO);

    BusinessCardDTO deleteBusinessCard(Long cardId);
}
