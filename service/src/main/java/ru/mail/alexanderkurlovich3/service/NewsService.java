package ru.mail.alexanderkurlovich3.service;


import ru.mail.alexanderkurlovich3.model.NewsDTO;

import java.util.List;

public interface NewsService {

    void createNews(NewsDTO newsDTO);

    List<Integer> getPages(int currentPage);

    List<NewsDTO> getNewsInPage(int currentPage);

    void deleteNews(Long newsId);

    NewsDTO getNewsById(Long newsId);

    void updateNews(Long newId, NewsDTO newsDTO);
}
