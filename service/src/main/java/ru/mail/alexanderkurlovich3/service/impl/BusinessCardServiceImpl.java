package ru.mail.alexanderkurlovich3.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.alexanderkurlovich3.dao.BusinessCardDao;
import ru.mail.alexanderkurlovich3.dao.UserDao;
import ru.mail.alexanderkurlovich3.dao.model.BusinessCard;
import ru.mail.alexanderkurlovich3.dao.model.User;
import ru.mail.alexanderkurlovich3.model.BusinessCardDTO;
import ru.mail.alexanderkurlovich3.service.BusinessCardService;
import ru.mail.alexanderkurlovich3.service.converter.impl.BusinessCardDTOConvertor;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;
import ru.mail.alexanderkurlovich3.service.util.UserPrincipalAccess;

import java.util.Collections;
import java.util.List;

@Service
public class BusinessCardServiceImpl implements BusinessCardService {

    private static final Logger logger = LogManager.getLogger(BusinessCardServiceImpl.class);
    private final UserDao userDao;
    private final BusinessCardDTOConvertor businessCardDTOConvertor;
    private final BusinessCardDao businessCardDao;
    private final UserPrincipalAccess userPrincipalAccess;

    @Autowired
    public BusinessCardServiceImpl(UserDao userDao, BusinessCardDTOConvertor businessCardDTOConvertor,
                                   BusinessCardDao businessCardDao, UserPrincipalAccess userPrincipalAccess) {
        this.userDao = userDao;
        this.businessCardDTOConvertor = businessCardDTOConvertor;
        this.businessCardDao = businessCardDao;
        this.userPrincipalAccess = userPrincipalAccess;
    }


    @Override
    @Transactional(readOnly = true)
    public List<BusinessCardDTO> getCardsForUser() {
        return getCardsForUser(userPrincipalAccess.getUserPrincipalId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<BusinessCardDTO> getCardsForUser(Long userId) {
        try {
            User user = userDao.findOne(userId);
            List<BusinessCard> cards = user.getBusinessCards();
            if (cards != null) {
                List<BusinessCardDTO> cardDTOS = businessCardDTOConvertor.toDTOList(cards);
                return cardDTOS;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.emptyList();
    }

    @Override
    @Transactional
    public void createNewBusinessCardForUser(BusinessCardDTO cardDTO) {
        try {
            User user = userDao.findOne(userPrincipalAccess.getUserPrincipalId());
            BusinessCard card = businessCardDTOConvertor.toEntity(cardDTO);
            card.setUser(user);
            businessCardDao.create(card);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public BusinessCardDTO deleteBusinessCard(Long cardId) {
        try {
            BusinessCard businessCard = businessCardDao.findOne(cardId);
            if (businessCard != null) {
                BusinessCardDTO cardDTO = businessCardDTOConvertor.toDTO(businessCard);
                businessCardDao.delete(businessCard);
                return cardDTO;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return new BusinessCardDTO();
    }


}
