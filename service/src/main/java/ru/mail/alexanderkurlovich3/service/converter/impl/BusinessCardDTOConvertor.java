package ru.mail.alexanderkurlovich3.service.converter.impl;

import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.dao.model.BusinessCard;
import ru.mail.alexanderkurlovich3.model.BusinessCardDTO;
import ru.mail.alexanderkurlovich3.service.converter.Converter;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;

@Component
public class BusinessCardDTOConvertor implements DTOConverter<BusinessCardDTO, BusinessCard>, Converter<BusinessCardDTO, BusinessCard>{

    @Override
    public BusinessCardDTO toDTO(BusinessCard entity) {
        BusinessCardDTO businessCard = new BusinessCardDTO();
        businessCard.setId(entity.getId());
        businessCard.setTitle(entity.getTitle());
        businessCard.setFullName(entity.getFullName());
        businessCard.setWorkingTelephone(entity.getWorkingTelephone());
        return businessCard;
    }

    @Override
    public BusinessCard toEntity(BusinessCardDTO dto) {
        BusinessCard card = new BusinessCard();
        card.setFullName(dto.getFullName());
        card.setTitle(dto.getTitle());
        card.setWorkingTelephone(dto.getWorkingTelephone());
        return card;
    }
}
