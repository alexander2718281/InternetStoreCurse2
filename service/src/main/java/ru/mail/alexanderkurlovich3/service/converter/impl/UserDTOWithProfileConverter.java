package ru.mail.alexanderkurlovich3.service.converter.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.dao.RoleDao;
import ru.mail.alexanderkurlovich3.dao.model.Profile;
import ru.mail.alexanderkurlovich3.dao.model.Role;
import ru.mail.alexanderkurlovich3.dao.model.User;
import ru.mail.alexanderkurlovich3.service.converter.Converter;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;
import ru.mail.alexanderkurlovich3.dao.impl.RoleDaoImpl;
import ru.mail.alexanderkurlovich3.model.*;

@Component
public class UserDTOWithProfileConverter implements DTOConverter<UserDTOWithProfile, User>, Converter<UserDTOWithProfile, User> {

    private final RoleDao roleDao;

    @Autowired
    public UserDTOWithProfileConverter(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public User toEntity(UserDTOWithProfile dto) {
        Role role = roleDao.getRoleByName(dto.getType());
        User user = new User();
        user.setEmail(dto.getEmail());
        user.setPassword(dto.getPassword());
        user.setSurename(dto.getSurename());
        user.setName(dto.getName());
        Profile profile = new Profile();
        profile.setAddress(dto.getAddress());
        profile.setTelephone(dto.getTelephone());
        user.setProfile(profile);
        profile.setUser(user);
        user.setRole(role);
        return user;
    }

    @Override
    public UserDTOWithProfile toDTO(User entity) {
        UserDTOWithProfile userDTOWithProfile = new UserDTOWithProfile();
        userDTOWithProfile.setId(entity.getId());
        userDTOWithProfile.setEmail(entity.getEmail());
        userDTOWithProfile.setSurename(entity.getSurename());
        userDTOWithProfile.setName(entity.getName());
        userDTOWithProfile.setType(entity.getRole().getName());
        userDTOWithProfile.setAddress(entity.getProfile().getAddress());
        userDTOWithProfile.setTelephone(entity.getProfile().getTelephone());
        userDTOWithProfile.setEnable(entity.isEnable());
        return userDTOWithProfile;
    }

}
