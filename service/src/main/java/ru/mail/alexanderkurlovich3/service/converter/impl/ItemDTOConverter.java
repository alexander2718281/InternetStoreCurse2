package ru.mail.alexanderkurlovich3.service.converter.impl;


import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.dao.model.Discount;
import ru.mail.alexanderkurlovich3.dao.model.Item;
import ru.mail.alexanderkurlovich3.model.ItemDTO;
import ru.mail.alexanderkurlovich3.service.converter.Converter;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;

import java.math.BigDecimal;

@Component
public class ItemDTOConverter implements Converter<ItemDTO, Item>, DTOConverter<ItemDTO, Item> {

    @Override
    public Item toEntity(ItemDTO dto) {
        Item item = new Item();
        item.setName(dto.getName());
        item.setDescription(dto.getDescription());
        item.setPrice(dto.getPrice());
        item.setUniqueNumber(dto.getUniqueNumber());
        return item;
    }

    @Override
    public ItemDTO toDTO(Item entity) {
        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(entity.getId());
        itemDTO.setName(entity.getName());
        itemDTO.setDescription(entity.getDescription());
        itemDTO.setUniqueNumber(entity.getUniqueNumber());
        itemDTO.setPrice(entity.getPrice());
        int summOfDiscount = 0;
        for (Discount discount : entity.getDiscounts()) {
            summOfDiscount += discount.getPercent();
        }
        if (summOfDiscount == 0) {
            itemDTO.setPriceWithDiscount(entity.getPrice());
        } else {
            itemDTO.setPriceWithDiscount(entity.getPrice().multiply(BigDecimal.valueOf(summOfDiscount)).divide(BigDecimal.valueOf(100)));
        }
        return itemDTO;
    }

}
