package ru.mail.alexanderkurlovich3.service;

import ru.mail.alexanderkurlovich3.model.OrderDTO;
import ru.mail.alexanderkurlovich3.model.OrderDTOWithUser;

import java.util.List;

public interface OrderService {

    void save(Long productId, int quantity);

    List<OrderDTO> getOrdersForCurrentUser();

    List<OrderDTOWithUser> getUndeliveredOrdersOnPage(int currentPage);

    void changeOrderStatus(Long orderId, String status);

    List<Integer> getPages(int currentPage);

    List<String> getStatses();

}
