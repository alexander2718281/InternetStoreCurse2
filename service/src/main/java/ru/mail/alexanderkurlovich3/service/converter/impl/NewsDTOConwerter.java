package ru.mail.alexanderkurlovich3.service.converter.impl;


import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.dao.model.News;
import ru.mail.alexanderkurlovich3.model.NewsDTO;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;

@Component
public class NewsDTOConwerter implements DTOConverter<NewsDTO, News> {

    @Override
    public NewsDTO toDTO(News entity) {
        NewsDTO newsDTO = new NewsDTO();
        newsDTO.setId(entity.getId());
        newsDTO.setTitle(entity.getTitle());
        newsDTO.setContent(entity.getContent());
        newsDTO.setCreated(entity.getCreated());
        newsDTO.setUserEmail(entity.getUser().getEmail());
        return newsDTO;
    }

}
