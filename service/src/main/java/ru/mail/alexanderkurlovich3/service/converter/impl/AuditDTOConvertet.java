package ru.mail.alexanderkurlovich3.service.converter.impl;


import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.dao.model.Audit;
import ru.mail.alexanderkurlovich3.model.AuditDTO;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;

import java.util.ArrayList;
import java.util.List;

@Component
public class AuditDTOConvertet implements DTOConverter<AuditDTO, Audit> {

    @Override
    public AuditDTO toDTO(Audit entity) {
        AuditDTO auditDTO = new AuditDTO();
        auditDTO.setId(entity.getId());
        auditDTO.setCreated(entity.getCreated());
        auditDTO.setEvent(entity.getEvent());
        auditDTO.setType(entity.getEventType().toString());
        auditDTO.setUserEmail(entity.getUser().getEmail());
        return auditDTO;
    }

    @Override
    public List<AuditDTO> toDTOList(List<Audit> entities) {
        List<AuditDTO> auditDTOS = new ArrayList<>();
        for (Audit entity : entities) {
            auditDTOS.add(toDTO(entity));
        }
        return auditDTOS;
    }
}
