package ru.mail.alexanderkurlovich3.service;


import ru.mail.alexanderkurlovich3.model.UserDTO;
import ru.mail.alexanderkurlovich3.model.UserDTOWithProfile;

import java.util.List;

public interface UserService {

    int register(UserDTOWithProfile user);

    Long getCountOfUsers();

    List<UserDTOWithProfile> getUsersInPage(int currentPage);

    void changeAbility(Long userId);

    void changeRole(Long userId, Long roleId);

    void deleteUser(Long userId);

    UserDTO getUserById(Long id);

    void changePassword(Long id, String newPassword);

    List<Integer> getPages(int currentPage);

    UserDTOWithProfile getAuthoriseUser();

    String updateUser(UserDTOWithProfile userDTO);

}
