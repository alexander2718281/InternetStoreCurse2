package ru.mail.alexanderkurlovich3.service.converter.impl;


import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.service.converter.Converter;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;
import ru.mail.alexanderkurlovich3.dao.model.Discount;
import ru.mail.alexanderkurlovich3.model.DiscountDTO;

@Component
public class DiscountDTOConverter implements Converter<DiscountDTO, Discount>, DTOConverter<DiscountDTO, Discount> {

    @Override
    public Discount toEntity(DiscountDTO dto) {
        Discount discount = new Discount();
        discount.setFinalDate(dto.getFinalDate());
        discount.setName(dto.getName());
        discount.setPercent(dto.getPercent());
        return discount;
    }

    @Override
    public DiscountDTO toDTO(Discount entity) {
        DiscountDTO discountDTO = new DiscountDTO();
        discountDTO.setId(entity.getId());
        discountDTO.setFinalDate(entity.getFinalDate());
        discountDTO.setName(entity.getName());
        discountDTO.setPercent(entity.getPercent());
        return discountDTO;
    }
}
