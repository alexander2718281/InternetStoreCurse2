package ru.mail.alexanderkurlovich3.service;

import ru.mail.alexanderkurlovich3.model.PermissionDTO;
import ru.mail.alexanderkurlovich3.model.RoleDTO;

import java.util.Set;

public interface PermissionService {

    Set<PermissionDTO> getPermissionsOfRole(Long roleId);
}
