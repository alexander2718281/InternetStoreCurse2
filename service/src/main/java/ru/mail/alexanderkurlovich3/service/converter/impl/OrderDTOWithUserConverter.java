package ru.mail.alexanderkurlovich3.service.converter.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.dao.model.Order;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;
import ru.mail.alexanderkurlovich3.model.*;

@Component
public class OrderDTOWithUserConverter implements DTOConverter<OrderDTOWithUser, Order> {

    private final ItemDTOConverter itemDTOConverter;
    private final UserDTOConverter userDTOConverter;

    @Autowired
    public OrderDTOWithUserConverter(ItemDTOConverter itemDTOConverter, UserDTOConverter userDTOConverter) {
        this.itemDTOConverter = itemDTOConverter;
        this.userDTOConverter = userDTOConverter;
    }

    @Override
    public OrderDTOWithUser toDTO(Order entity) {
        OrderDTOWithUser orderDTO = new OrderDTOWithUser();
        orderDTO.setItemName(entity.getItem().getName());
        orderDTO.setCreated(entity.getCreated());
        orderDTO.setQuantity(entity.getQuantity());
        orderDTO.setId(entity.getId());
        orderDTO.setStatus(entity.getStatus().name());
        orderDTO.setUserEmail(entity.getUser().getEmail());
        orderDTO.setSumm(entity.getSumm());
        orderDTO.setSummWithDiscount(entity.getSummWithDiscount());
        return orderDTO;
    }
}
