package ru.mail.alexanderkurlovich3.service.converter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;
import ru.mail.alexanderkurlovich3.dao.model.Comment;
import ru.mail.alexanderkurlovich3.model.CommentDTO;
import ru.mail.alexanderkurlovich3.model.UserDTO;

@Component
public class CommentDTOConverter implements DTOConverter<CommentDTO, Comment> {

    @Autowired
    public CommentDTOConverter() {
    }

    @Override
    public CommentDTO toDTO(Comment entity) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setContent(entity.getContent());
        commentDTO.setCreated(entity.getCreated());
        commentDTO.setId(entity.getId());
        commentDTO.setUserEmail(entity.getUser().getEmail());
        return commentDTO;
    }
}
