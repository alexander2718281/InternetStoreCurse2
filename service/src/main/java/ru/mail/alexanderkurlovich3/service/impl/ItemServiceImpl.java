package ru.mail.alexanderkurlovich3.service.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.mail.alexanderkurlovich3.dao.DiscountDao;
import ru.mail.alexanderkurlovich3.dao.ItemDao;
import ru.mail.alexanderkurlovich3.dao.model.Item;
import ru.mail.alexanderkurlovich3.model.ItemDTO;
import ru.mail.alexanderkurlovich3.service.ItemService;
import ru.mail.alexanderkurlovich3.service.converter.impl.ItemDTOConverter;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;
import ru.mail.alexanderkurlovich3.service.properties.PagesProperties;
import ru.mail.alexanderkurlovich3.service.util.PaginationHalper;
import ru.mail.alexanderkurlovich3.service.util.SAXParserUtil;

import java.util.Collections;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    private static final Logger logger = LogManager.getLogger(ItemServiceImpl.class);
    private final ItemDao itemDao;
    private final ItemDTOConverter itemDTOConverter;
    private final DiscountDao discountDao;
    private final PaginationHalper paginationHalper;
    private final PagesProperties pagesProperties;
    private final SAXParserUtil saxParser;

    @Autowired
    public ItemServiceImpl(ItemDao itemDao, ItemDTOConverter itemDTOConverter, DiscountDao discountDao,
                           PaginationHalper paginationHalper, PagesProperties pagesProperties, SAXParserUtil saxParser) {
        this.itemDao = itemDao;
        this.itemDTOConverter = itemDTOConverter;
        this.discountDao = discountDao;
        this.paginationHalper = paginationHalper;
        this.pagesProperties = pagesProperties;
        this.saxParser = saxParser;
    }

    @Override
    @Transactional
    public boolean updateProducts(MultipartFile file) {
        List<Item> items = itemDTOConverter.toEntityList(saxParser.saxParser(file));
        if (items != null) {
            try {
                for (Item item : items) {
                    Item itemFromTable = itemDao.getByNumber(item.getUniqueNumber());
                    if (itemFromTable == null) {
                        itemDao.create(item);
                    } else if (item.equals(itemFromTable)) {
                        continue;
                    } else {
                        itemFromTable.setName(item.getName());
                        itemFromTable.setDescription(item.getDescription());
                        itemFromTable.setPrice(item.getPrice());
                        itemDao.update(itemFromTable);
                    }
                }
                return true;
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw new ServiceException(e);
            }
        }
        return false;
    }


    @Override
    @Transactional
    public void deleteById(Long itemId) {
        try {
            Item item = itemDao.findOne(itemId);
            itemDao.delete(item);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }


    @Override
    @Transactional(readOnly = true)
    public List<Integer> getPages(int currentPage) {
        try {
            Long countOfItems = itemDao.getCount();
            List<Integer> pages = paginationHalper.getPages(currentPage, countOfItems, pagesProperties.getCountOfItemsOnPage());
            if (pages != null) {
                return pages;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        return Collections.emptyList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<ItemDTO> getItemsInPage(int currentPage) {
        try {
            int startOfPage = paginationHalper.getStartOfPage(currentPage, pagesProperties.getCountOfItemsOnPage());
            List<Item> items = itemDao.getItemsInPage(startOfPage, pagesProperties.getCountOfUsersInPage());
            if (items != null) {
                List<ItemDTO> dtos = itemDTOConverter.toDTOList(items);
                return dtos;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        return Collections.EMPTY_LIST;
    }

    @Override
    @Transactional
    public void copyItem(Long itemId, String newUniqueNumber) {
        try {
            Item originalItem = itemDao.findOne(itemId);
            Item newItem = new Item();
            newItem.setPrice(originalItem.getPrice());
            newItem.setDescription(originalItem.getDescription());
            newItem.setName(originalItem.getName());
            newItem.setUniqueNumber(newUniqueNumber);
            itemDao.create(newItem);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    @Transactional
    public ItemDTO createItemFromAPI(ItemDTO item) {
        try {
            Item dbItem = itemDao.getByNumber(item.getUniqueNumber());
            if (dbItem == null) {
                Item newItem = itemDTOConverter.toEntity(item);
                itemDao.create(newItem);
                item.setId(newItem.getId());
                return item;
            } else {
                return new ItemDTO();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public ItemDTO getItemById(Long itemId) {
        try {
            Item item = itemDao.findOne(itemId);
            if (item != null) {
                return itemDTOConverter.toDTO(item);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        return new ItemDTO();
    }

    @Override
    @Transactional
    public ItemDTO updateProductWithAPI(ItemDTO itemDTO) {
        try {
            Item item = itemDao.findOne(itemDTO.getId());
            if (item != null) {
                item.setName(itemDTO.getName());
                item.setUniqueNumber(itemDTO.getUniqueNumber());
                item.setDescription(itemDTO.getDescription());
                item.setPrice(itemDTO.getPrice());
                itemDao.update(item);
                return itemDTO;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        return new ItemDTO();
    }

    @Override
    @Transactional
    public ItemDTO deleteByIdWithAPI(Long itemId) {
        try {
            Item item = itemDao.findOne(itemId);
            if (item != null) {
                ItemDTO itemDTO = itemDTOConverter.toDTO(item);
                itemDao.delete(item);
                return itemDTO;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
        return new ItemDTO();
    }

    @Override
    @Transactional
    public boolean createNewItem(ItemDTO itemDTO) {
        try {
            Item item = itemDao.getByNumber(itemDTO.getUniqueNumber());
            if (item == null){
                item = itemDTOConverter.toEntity(itemDTO);
                itemDao.create(item);
                return true;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
        return false;
    }
}
