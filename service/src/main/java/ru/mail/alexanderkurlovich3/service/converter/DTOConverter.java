package ru.mail.alexanderkurlovich3.service.converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface DTOConverter<D, E> {

    D toDTO(E entity);

    default List<D> toDTOList(List<E> entities) {
        List<D> dtos = new ArrayList<D>();
        for (E entity : entities) {
            dtos.add(toDTO(entity));
        }
        return dtos;
    }

    default Set<D> toDTOSet(Set<E> entities){
        Set<D> dtos = new HashSet<D>();
        for (E entity : entities) {
            dtos.add(toDTO(entity));
        }
        return dtos;
    }
}
