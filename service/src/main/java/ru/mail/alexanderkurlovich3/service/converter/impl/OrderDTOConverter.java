package ru.mail.alexanderkurlovich3.service.converter.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;
import ru.mail.alexanderkurlovich3.dao.model.Order;
import ru.mail.alexanderkurlovich3.model.OrderDTO;
import ru.mail.alexanderkurlovich3.model.ItemDTO;

@Component
public class OrderDTOConverter implements DTOConverter<OrderDTO, Order> {

    private final ItemDTOConverter itemDTOConverter;

    @Autowired
    public OrderDTOConverter(ItemDTOConverter itemDTOConverter) {
        this.itemDTOConverter = itemDTOConverter;
    }

    @Override
    public OrderDTO toDTO(Order entity) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setItemName(entity.getItem().getName());
        orderDTO.setCreated(entity.getCreated());
        orderDTO.setQuantity(entity.getQuantity());
        orderDTO.setId(entity.getId());
        orderDTO.setStatus(entity.getStatus().name());
        orderDTO.setSumm(entity.getSumm());
        orderDTO.setSummWithDiscount(entity.getSummWithDiscount());
        return orderDTO;
    }

}
