package ru.mail.alexanderkurlovich3.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.alexanderkurlovich3.dao.UserDao;
import ru.mail.alexanderkurlovich3.dao.model.User;
import ru.mail.alexanderkurlovich3.model.UserPrincipal;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService{

    private final UserDao userDao;

    @Autowired
    public UserDetailsServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) {
        User user = userDao.getUserByEmail(email);
        UserPrincipal userPrincipal;
        if (user != null){
            userPrincipal = new UserPrincipal(user);
        }
        else {
            throw new UsernameNotFoundException("User not found");
        }
        return userPrincipal;
    }
}
