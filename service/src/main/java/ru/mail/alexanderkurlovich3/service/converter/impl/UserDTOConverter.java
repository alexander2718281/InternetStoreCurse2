package ru.mail.alexanderkurlovich3.service.converter.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.dao.RoleDao;
import ru.mail.alexanderkurlovich3.dao.model.Role;
import ru.mail.alexanderkurlovich3.dao.model.User;
import ru.mail.alexanderkurlovich3.service.converter.Converter;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;
import ru.mail.alexanderkurlovich3.dao.impl.RoleDaoImpl;
import ru.mail.alexanderkurlovich3.model.*;

@Component
public class UserDTOConverter implements DTOConverter<UserDTO, User>, Converter<UserDTO, User> {

    private final RoleDao roleDao;

    @Autowired
    public UserDTOConverter(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public UserDTO toDTO(User entity) {
        UserDTO userDTO = new UserDTO(entity.getId(), entity.getSurename(), entity.getName(), entity.getEmail(), entity.getRole().getName());
        return userDTO;
    }

    @Override
    public User toEntity(UserDTO dto) {
        Role role = roleDao.getRoleByName(dto.getType());
        User user = new User();
        user.setId(dto.getId());
        user.setEmail(dto.getEmail());
        user.setSurename(dto.getSurename());
        user.setName(dto.getName());
        user.setRole(role);
        return user;
    }
}
