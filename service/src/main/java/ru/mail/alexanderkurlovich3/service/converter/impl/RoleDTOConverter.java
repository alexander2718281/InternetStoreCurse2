package ru.mail.alexanderkurlovich3.service.converter.impl;

import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.service.converter.Converter;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;
import ru.mail.alexanderkurlovich3.dao.model.Role;
import ru.mail.alexanderkurlovich3.model.RoleDTO;

@Component
public class RoleDTOConverter implements Converter<RoleDTO, Role>, DTOConverter<RoleDTO, Role> {

    @Override
    public Role toEntity(RoleDTO dto) {
        Role role = new Role();
        role.setId(dto.getId());
        role.setName(dto.getName());
        return role;
    }

    @Override
    public RoleDTO toDTO(Role entity) {
        RoleDTO dto = new RoleDTO(entity.getId(), entity.getName());
        return dto;
    }
}
