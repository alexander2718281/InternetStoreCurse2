package ru.mail.alexanderkurlovich3.service.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.alexanderkurlovich3.dao.AuditDao;
import ru.mail.alexanderkurlovich3.dao.UserDao;
import ru.mail.alexanderkurlovich3.service.AuditService;
import ru.mail.alexanderkurlovich3.service.converter.impl.AuditDTOConvertet;
import ru.mail.alexanderkurlovich3.dao.model.Audit;
import ru.mail.alexanderkurlovich3.model.AuditDTO;
import ru.mail.alexanderkurlovich3.dao.model.EventType;
import ru.mail.alexanderkurlovich3.dao.model.User;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;
import ru.mail.alexanderkurlovich3.service.properties.PagesProperties;
import ru.mail.alexanderkurlovich3.service.util.PaginationHalper;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class AuditServiceImpl implements AuditService {
    
    private static final Logger logger = LogManager.getLogger(AuditServiceImpl.class);
    private final AuditDao auditDao;
    private final UserDao userDao;
    private final AuditDTOConvertet auditDTOConvertet;
    private final PaginationHalper paginationHalper;
    private final PagesProperties pagesProperties;

    @Autowired
    public AuditServiceImpl(AuditDao auditDao, UserDao userDao, AuditDTOConvertet auditDTOConvertet,
                            PaginationHalper paginationHalper, PagesProperties pagesProperties) {
        this.auditDao = auditDao;
        this.userDao = userDao;
        this.auditDTOConvertet = auditDTOConvertet;
        this.paginationHalper = paginationHalper;
        this.pagesProperties = pagesProperties;
    }

    @Override
    @Transactional
    public void createEvent(long userId, String event, EventType type) {
        User user = userDao.findOne(userId);
        createEvent(user, event, type);

    }

    @Override
    @Transactional
    public void createEvent(User user, String event, EventType type) {
        LocalDateTime currenrTime = LocalDateTime.now();
        Instant instant = currenrTime.atZone(ZoneId.systemDefault()).toInstant();
        Date date = Date.from(instant);
        Audit audit = new Audit(event, date, user, type);
        auditDao.create(audit);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AuditDTO> getAuditsInPage(int currentPage) {
        try {
            int startOfPage = paginationHalper.getStartOfPage(currentPage, pagesProperties.getCountOfAuditsInPage());
            List<Audit> audits = auditDao.getAuditsInPage(startOfPage, pagesProperties.getCountOfAuditsInPage());
            List<AuditDTO> auditDTOS = auditDTOConvertet.toDTOList(audits);
            return auditDTOS;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Integer> getPages(int currentPage) {
        try {
            Long countOfAudits = auditDao.getCount();
            List<Integer> pages = paginationHalper.getPages(currentPage, countOfAudits, pagesProperties.getCountOfAuditsInPage());
            if (pages != null) {
                return pages;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.emptyList();
    }
}
