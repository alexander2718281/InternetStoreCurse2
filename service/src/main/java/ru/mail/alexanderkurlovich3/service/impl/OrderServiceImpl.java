package ru.mail.alexanderkurlovich3.service.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.alexanderkurlovich3.dao.DiscountDao;
import ru.mail.alexanderkurlovich3.dao.OrderDao;
import ru.mail.alexanderkurlovich3.dao.ItemDao;
import ru.mail.alexanderkurlovich3.dao.UserDao;
import ru.mail.alexanderkurlovich3.dao.model.*;
import ru.mail.alexanderkurlovich3.service.AuditService;
import ru.mail.alexanderkurlovich3.service.OrderService;
import ru.mail.alexanderkurlovich3.service.converter.impl.OrderDTOConverter;
import ru.mail.alexanderkurlovich3.service.converter.impl.OrderDTOWithUserConverter;
import ru.mail.alexanderkurlovich3.model.*;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;
import ru.mail.alexanderkurlovich3.service.properties.PagesProperties;
import ru.mail.alexanderkurlovich3.service.util.PaginationHalper;
import ru.mail.alexanderkurlovich3.service.util.UserPrincipalAccess;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LogManager.getLogger(OrderServiceImpl.class);
    private final OrderDao orderDao;
    private final ItemDao itemDao;
    private final UserDao userDao;
    private final DiscountDao discountDao;
    private final OrderDTOConverter orderDTOConverter;
    private final OrderDTOWithUserConverter orderDTOWithUserConverter;
    private final AuditService auditService;
    private final UserPrincipalAccess userPrincipalAccess;
    private final PaginationHalper paginationHalper;
    private final PagesProperties pagesProperties;

    @Autowired
    public OrderServiceImpl(OrderDao orderDao, ItemDao itemDao, UserDao userDao, DiscountDao discountDao,
                            OrderDTOConverter orderDTOConverter, OrderDTOWithUserConverter orderDTOWithUserConverter,
                            AuditService auditService, UserPrincipalAccess userPrincipalAccess,
                            PaginationHalper paginationHalper, PagesProperties pagesProperties) {
        this.orderDao = orderDao;
        this.itemDao = itemDao;
        this.userDao = userDao;
        this.discountDao = discountDao;
        this.orderDTOConverter = orderDTOConverter;
        this.orderDTOWithUserConverter = orderDTOWithUserConverter;
        this.auditService = auditService;
        this.userPrincipalAccess = userPrincipalAccess;
        this.paginationHalper = paginationHalper;
        this.pagesProperties = pagesProperties;
    }

    @Override
    @Transactional
    public void save(Long productId, int quantity) {
        try {
            Item item = itemDao.findOne(productId);
            User user = userDao.findOne(userPrincipalAccess.getUserPrincipalId());
            int resultDiscount = 0;
            ;
            for (Discount discount : item.getDiscounts()) {
                resultDiscount += discount.getPercent();
            }
            if (user.getDiscount() != null) {
                resultDiscount += user.getDiscount().getPercent();
            }
            Order order = orderDao.getNewOrderByIds(item, user);
            if (order == null) {
                LocalDateTime currenrTime = LocalDateTime.now();
                Instant instant = currenrTime.atZone(ZoneId.systemDefault()).toInstant();
                Date date = Date.from(instant);
                order = new Order();
                order.setCreated(date);
                order.setItem(item);
                order.setUser(user);
                order.setQuantity(quantity);
                order.setStatus(OrderStatus.NEW);
                order.setSumm(item.getPrice().multiply(BigDecimal.valueOf(quantity)));
                if (resultDiscount != 0) {
                    order.setSummWithDiscount(item.getPrice().multiply(BigDecimal.valueOf(quantity)).multiply(BigDecimal
                            .valueOf(resultDiscount)).divide(BigDecimal.valueOf(100)));
                } else {
                    order.setSummWithDiscount(order.getSumm());
                }
                user.getOrders().add(order);
                orderDao.create(order);
            } else {
                order.setQuantity(order.getQuantity() + quantity);
                order.setSumm(order.getSumm().add(item.getPrice().multiply(BigDecimal.valueOf(quantity))));
                if (resultDiscount != 0) {
                    order.setSummWithDiscount(order.getSummWithDiscount().add(item.getPrice().multiply(BigDecimal
                            .valueOf(quantity)).multiply(BigDecimal.valueOf(resultDiscount)).divide(BigDecimal.valueOf(100))));
                } else {
                    order.setSummWithDiscount(order.getSummWithDiscount().add(item.getPrice().multiply(BigDecimal.valueOf(quantity))));
                }
            }
            auditService.createEvent(user, "order " + item.getName(), EventType.ORDER);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public List<OrderDTO> getOrdersForCurrentUser() {
        try {
            User user = userDao.findOne(userPrincipalAccess.getUserPrincipalId());
            List<Order> orders = orderDao.getOrdersOfUser(user);
            if (orders != null) {
                List<OrderDTO> orderDTOS = orderDTOConverter.toDTOList(orders);
                return orderDTOS;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.emptyList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderDTOWithUser> getUndeliveredOrdersOnPage(int currentPage) {
        try {
            int startOfPage = paginationHalper.getStartOfPage(currentPage, pagesProperties.getCountOfOrdersOnPage());
            List<Order> users = orderDao.getUndeliveredOrdersInPage(startOfPage, pagesProperties.getCountOfOrdersOnPage());
            if (users != null) {
                List<OrderDTOWithUser> orderDTOs = orderDTOWithUserConverter.toDTOList(users);
                return orderDTOs;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.EMPTY_LIST;
    }

    @Override
    @Transactional
    public void changeOrderStatus(Long orderId, String status) {
        try {
            Order order = orderDao.findOne(orderId);
            if (order != null) {
                OrderStatus newStatus = OrderStatus.valueOf(status);
                order.setStatus(newStatus);
                orderDao.update(order);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Integer> getPages(int currentPage) {
        try {
            Long countOfUser = orderDao.getCount();
            List<Integer> pages = paginationHalper.getPages(currentPage, countOfUser, pagesProperties.getCountOfOrdersOnPage());
            if (pages != null) {
                return pages;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.emptyList();
    }

    @Override
    public List<String> getStatses() {
        List<OrderStatus> statuses = Arrays.asList(OrderStatus.class.getEnumConstants());
        List<String> statusNames = new ArrayList<>();
        for (OrderStatus status : statuses) {
            statusNames.add(status.name());
        }
        return statusNames;
    }

}
