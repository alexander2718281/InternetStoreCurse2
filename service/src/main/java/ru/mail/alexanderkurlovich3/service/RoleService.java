package ru.mail.alexanderkurlovich3.service;

import ru.mail.alexanderkurlovich3.model.RoleDTO;

import java.util.List;

public interface RoleService {

    List<RoleDTO> getAllRoles();

}
