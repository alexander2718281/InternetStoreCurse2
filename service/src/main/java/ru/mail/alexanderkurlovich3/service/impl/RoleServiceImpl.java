package ru.mail.alexanderkurlovich3.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.alexanderkurlovich3.dao.RoleDao;
import ru.mail.alexanderkurlovich3.service.RoleService;
import ru.mail.alexanderkurlovich3.service.converter.impl.RoleDTOConverter;
import ru.mail.alexanderkurlovich3.dao.model.Role;
import ru.mail.alexanderkurlovich3.model.RoleDTO;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private static final Logger logger = LogManager.getLogger(RoleServiceImpl.class);

    private final RoleDao roleDao;
    private final RoleDTOConverter roleDTOConverter;

    @Autowired
    public RoleServiceImpl(RoleDao roleDao, RoleDTOConverter roleDTOConverter) {
        this.roleDao = roleDao;
        this.roleDTOConverter = roleDTOConverter;
    }

    @Override
    @Transactional
    public List<RoleDTO> getAllRoles() {
        List<RoleDTO> roleDTOS;
        try {
            List<Role> roles = roleDao.getAll();
            roleDTOS = roleDTOConverter.toDTOList(roles);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return roleDTOS;
    }


}
