package ru.mail.alexanderkurlovich3.service.util;


import javafx.util.converter.BigDecimalStringConverter;
import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.mail.alexanderkurlovich3.model.ItemDTO;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductHandler extends DefaultHandler {

    private boolean bname = false;
    private boolean bnumber = false;
    private boolean binfo = false;
    private boolean bprice = false;

    private ItemDTO itemDTO = null;

    private  List<ItemDTO> products;

    @Override
    public void startDocument() throws SAXException {
        products = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("name")) {
            bname = true;
        } else if (qName.equalsIgnoreCase("number")) {
            bnumber = true;
        } else if (qName.equalsIgnoreCase("price")) {
            bprice = true;
        } else if (qName.equalsIgnoreCase("info")) {
            binfo = true;
        } else if (qName.equalsIgnoreCase("product")){
            itemDTO = new ItemDTO();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("product")) {
            products.add(itemDTO);

        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (bname) {
            itemDTO.setName(new String(ch, start, length));
            bname = false;
        } else if (bnumber) {
            itemDTO.setUniqueNumber(new String(ch, start, length));
            bnumber = false;
        } else if (binfo) {
            itemDTO.setDescription(new String(ch, start, length));
            binfo = false;
        } else if (bprice) {
            BigDecimalStringConverter bigDecimalStringConverter = new BigDecimalStringConverter();
            itemDTO.setPrice(bigDecimalStringConverter.fromString(new String(ch, start, length)));
            bprice = false;
        }
    }

    public List<ItemDTO> getProducts() {
        return products;
    }
}
