package ru.mail.alexanderkurlovich3.service.util;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PaginationHalper {

    @Autowired
    public PaginationHalper() {
    }

    public List<Integer> getPages(int currentPage, Long numOfFields, int numOfFieldsOnPage) {
        int numOfPages = (int) Math.ceil((double)numOfFields / numOfFieldsOnPage);
        List<Integer> pages = new ArrayList<>();
        if (currentPage - 2 > 0) {
            pages.add(currentPage - 2);
        }
        if (currentPage - 1 > 0) {
            pages.add(currentPage - 1);
        }
        pages.add(currentPage);
        if (currentPage + 1 <= numOfPages) {
            pages.add(currentPage + 1);
            if (currentPage + 2 <= numOfPages) {
                pages.add(currentPage + 2);
            }
        }
        return pages;
    }

    public int getStartOfPage(int page, int numOfFieldsOnPage){
        int start = numOfFieldsOnPage*(page - 1);
        return start;
    }
}
