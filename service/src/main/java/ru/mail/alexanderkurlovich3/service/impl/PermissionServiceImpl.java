package ru.mail.alexanderkurlovich3.service.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.alexanderkurlovich3.service.PermissionService;
import ru.mail.alexanderkurlovich3.dao.PermisssionDao;
import ru.mail.alexanderkurlovich3.dao.RoleDao;
import ru.mail.alexanderkurlovich3.service.converter.impl.PermissionConverter;
import ru.mail.alexanderkurlovich3.dao.model.Permission;
import ru.mail.alexanderkurlovich3.model.PermissionDTO;
import ru.mail.alexanderkurlovich3.dao.model.Role;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;

import java.util.Set;

@Service
public class PermissionServiceImpl implements PermissionService {

    private static final Logger logger = LogManager.getLogger(PermissionServiceImpl.class);
    private final RoleDao roleDao;
    private final PermisssionDao permisssionDao;
    private final PermissionConverter permissionConverter;

    @Autowired
    public PermissionServiceImpl(RoleDao roleDao, PermisssionDao permisssionDao, PermissionConverter permissionConverter) {
        this.roleDao = roleDao;
        this.permisssionDao = permisssionDao;
        this.permissionConverter = permissionConverter;
    }

    @Override
    @Transactional
    public Set<PermissionDTO> getPermissionsOfRole(Long roleId) {
        try {
            Set<PermissionDTO> permissionDTOS;
            Role role = roleDao.findOne(roleId);
            Set<Permission> permissions = role.getPermissions();
            permissionDTOS = permissionConverter.toDTOSet(permissions);
            return permissionDTOS;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }

    }
}
