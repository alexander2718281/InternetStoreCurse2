package ru.mail.alexanderkurlovich3.service.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.alexanderkurlovich3.dao.DiscountDao;
import ru.mail.alexanderkurlovich3.dao.ItemDao;
import ru.mail.alexanderkurlovich3.dao.UserDao;
import ru.mail.alexanderkurlovich3.dao.model.Discount;
import ru.mail.alexanderkurlovich3.dao.model.Item;
import ru.mail.alexanderkurlovich3.model.DiscountDTO;
import ru.mail.alexanderkurlovich3.service.AuditService;
import ru.mail.alexanderkurlovich3.service.DiscountService;
import ru.mail.alexanderkurlovich3.service.converter.impl.DiscountDTOConverter;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;

import java.util.Collections;
import java.util.List;

@Service
public class DiscountServiceImpl implements DiscountService {

    private static final Logger logger = LogManager.getLogger(DiscountServiceImpl.class);
    private final DiscountDao discountDao;
    private final ItemDao itemDao;
    private final DiscountDTOConverter discountDTOConverter;
    private final UserDao userDao;
    private final AuditService auditService;

    @Autowired
    public DiscountServiceImpl(DiscountDao discountDao, ItemDao itemDao, DiscountDTOConverter discountDTOConverter,
                               UserDao userDao, AuditService auditService) {
        this.discountDao = discountDao;
        this.itemDao = itemDao;
        this.discountDTOConverter = discountDTOConverter;
        this.userDao = userDao;
        this.auditService = auditService;
    }

    @Override
    @Transactional
    public void addDiscountToItem(Long itemId, Long discountId) {
        try {
            Item item = itemDao.findOne(itemId);
            Discount discount = discountDao.findOne(discountId);
            item.getDiscounts().add(discount);
            discount.getItems().add(item);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public List<DiscountDTO> getActualDiscounts() {
        try {
            List<Discount> discounts = discountDao.getAll();
            if (discounts != null) {
                List<DiscountDTO> discountDTOS = discountDTOConverter.toDTOList(discounts);
                return discountDTOS;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.emptyList();
    }
}
