package ru.mail.alexanderkurlovich3.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.alexanderkurlovich3.dao.RoleDao;
import ru.mail.alexanderkurlovich3.dao.UserDao;
import ru.mail.alexanderkurlovich3.dao.model.EventType;
import ru.mail.alexanderkurlovich3.dao.model.Role;
import ru.mail.alexanderkurlovich3.dao.model.User;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;
import ru.mail.alexanderkurlovich3.model.UserDTO;
import ru.mail.alexanderkurlovich3.model.UserDTOWithProfile;
import ru.mail.alexanderkurlovich3.service.AuditService;
import ru.mail.alexanderkurlovich3.service.UserService;
import ru.mail.alexanderkurlovich3.service.converter.impl.UserDTOConverter;
import ru.mail.alexanderkurlovich3.service.converter.impl.UserDTOWithProfileConverter;
import ru.mail.alexanderkurlovich3.service.properties.PagesProperties;
import ru.mail.alexanderkurlovich3.service.util.PaginationHalper;
import ru.mail.alexanderkurlovich3.service.util.UserPrincipalAccess;

import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

    private final UserDao userDao;
    private final UserDTOConverter userDTOConverter;
    private final UserDTOWithProfileConverter userDTOWithProfileConverter;
    private final AuditService auditService;
    private final RoleDao roleDao;
    private final BCryptPasswordEncoder passwordEncoder;
    private final PaginationHalper paginationHalper;
    private final PagesProperties pagesProperties;
    private final UserPrincipalAccess userPrincipalAccess;


    @Autowired
    public UserServiceImpl(UserDao userDao, UserDTOConverter userDTOConverter,
                           UserDTOWithProfileConverter userDTOWithProfileConverter, AuditService auditService, RoleDao roleDao,
                           BCryptPasswordEncoder passwordEncoder, PaginationHalper paginationHalper,
                           PagesProperties pagesProperties, UserPrincipalAccess userPrincipalAccess) {
        this.userDao = userDao;
        this.userDTOConverter = userDTOConverter;
        this.userDTOWithProfileConverter = userDTOWithProfileConverter;
        this.auditService = auditService;
        this.roleDao = roleDao;
        this.passwordEncoder = passwordEncoder;
        this.paginationHalper = paginationHalper;
        this.pagesProperties = pagesProperties;
        this.userPrincipalAccess = userPrincipalAccess;
    }

    @Override
    @Transactional
    public int register(UserDTOWithProfile userDTOWithProfile) {
        int res = 0;
        boolean isExists;
        try {
            isExists = userDao.isExists(userDTOWithProfile.getEmail());
            if (!isExists) {
                userDTOWithProfile.setType("CUSTOMER");
                userDTOWithProfile.setPassword(passwordEncoder.encode(userDTOWithProfile.getPassword()));
                userDao.create(userDTOWithProfileConverter.toEntity(userDTOWithProfile));
                res = 1;
                User userForAudit = userDao.getUserByEmail(userDTOWithProfile.getEmail());
                auditService.createEvent(userForAudit, "registration", EventType.USER);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return res;
    }

    @Override
    @Transactional
    public Long getCountOfUsers() {
        Long countOfUsers = userDao.getCount();
        return countOfUsers;
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserDTOWithProfile> getUsersInPage(int currentPage) {
        try {
            int startOfPage = paginationHalper.getStartOfPage(currentPage, pagesProperties.getCountOfUsersInPage());
            List<User> users = userDao.getUsersInPage(startOfPage, pagesProperties.getCountOfUsersInPage());
            if (users != null) {
                List<UserDTOWithProfile> dtos = userDTOWithProfileConverter.toDTOList(users);
                return dtos;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.EMPTY_LIST;
    }

    @Override
    @Transactional
    public void changeAbility(Long userId) {
        try {
            User user = userDao.findOne(userId);
            if (user.isEnable()) {
                user.setEnable(false);
            } else {
                user.setEnable(true);
            }
            userDao.update(user);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public void changeRole(Long userId, Long roleId) {
        try {
            User user = userDao.findOne(userId);
            Role role = roleDao.findOne(roleId);
            user.setRole(role);
            userDao.update(user);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public void deleteUser(Long userId) {
        try {
            User user = userDao.findOne(userId);
            userDao.delete(user);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO getUserById(Long id) {
        User user = userDao.findOne(id);
        UserDTO userDTO = userDTOConverter.toDTO(user);
        return userDTO;
    }

    @Override
    @Transactional
    public void changePassword(Long id, String newPassword) {
        try {
            User user = userDao.findOne(id);
            newPassword = passwordEncoder.encode(newPassword);
            user.setPassword(newPassword);
            userDao.update(user);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Integer> getPages(int currentPage) {
        try {
            Long countOfUser = getCountOfUsers();
            List<Integer> pages = paginationHalper.getPages(currentPage, countOfUser, pagesProperties.getCountOfUsersInPage());
            if (pages != null) {
                return pages;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.emptyList();
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTOWithProfile getAuthoriseUser() {
        try {
            User user = userDao.findOne(userPrincipalAccess.getUserPrincipalId());
            if (user != null) {
                UserDTOWithProfile userDTO = userDTOWithProfileConverter.toDTO(user);
                return userDTO;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return null;
    }

    @Override
    @Transactional
    public String updateUser(UserDTOWithProfile userDTO) {
        try {
            User user = userDao.findOne(userPrincipalAccess.getUserPrincipalId());
            if (user != null) {
                if (userDTO.getPassword() != null && !userDTO.getPassword().equals("")){
                    user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
                }
                user.setName(userDTO.getName());
                user.setSurename(userDTO.getSurename());
                user.getProfile().setAddress(userDTO.getAddress());
                user.getProfile().setTelephone(userDTO.getTelephone());
                userDao.update(user);
                return user.getRole().getName();
            }
        }catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return "";
    }
}