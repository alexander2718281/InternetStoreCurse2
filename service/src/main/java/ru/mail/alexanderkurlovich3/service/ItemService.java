package ru.mail.alexanderkurlovich3.service;

import org.springframework.web.multipart.MultipartFile;
import ru.mail.alexanderkurlovich3.model.ItemDTO;

import java.util.List;

public interface ItemService {

    boolean updateProducts(MultipartFile file);

    void deleteById(Long itemId);

    List<Integer> getPages(int currentPage);

    List<ItemDTO> getItemsInPage(int currentPage);

    void copyItem(Long itemId, String newUniqueNumber);

    ItemDTO createItemFromAPI(ItemDTO item);

    ItemDTO getItemById(Long itemId);

    ItemDTO updateProductWithAPI(ItemDTO itemDTO);

    ItemDTO deleteByIdWithAPI(Long itemId);

    boolean createNewItem(ItemDTO itemDTO);

}
