package ru.mail.alexanderkurlovich3.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.alexanderkurlovich3.dao.CommentDao;
import ru.mail.alexanderkurlovich3.dao.NewsDao;
import ru.mail.alexanderkurlovich3.dao.UserDao;
import ru.mail.alexanderkurlovich3.dao.model.Comment;
import ru.mail.alexanderkurlovich3.dao.model.EventType;
import ru.mail.alexanderkurlovich3.dao.model.News;
import ru.mail.alexanderkurlovich3.dao.model.User;
import ru.mail.alexanderkurlovich3.service.AuditService;
import ru.mail.alexanderkurlovich3.service.CommentService;
import ru.mail.alexanderkurlovich3.service.converter.impl.CommentDTOConverter;
import ru.mail.alexanderkurlovich3.model.*;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;
import ru.mail.alexanderkurlovich3.service.properties.PagesProperties;
import ru.mail.alexanderkurlovich3.service.util.PaginationHalper;
import ru.mail.alexanderkurlovich3.service.util.UserPrincipalAccess;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private static final Logger logger = LogManager.getLogger(CommentServiceImpl.class);
    private final CommentDao commentDao;
    private final UserDao userDao;
    private final NewsDao newsDao;
    private final AuditService auditService;
    private final CommentDTOConverter commentDTOConverter;
    private final PaginationHalper paginationHalper;
    private final PagesProperties pagesProperties;
    private final UserPrincipalAccess userPrincipalAccess;


    @Autowired
    public CommentServiceImpl(CommentDao commentDao, UserDao userDao, NewsDao newsDao,
                              AuditService auditService, CommentDTOConverter commentDTOConverter,
                              PaginationHalper paginationHalper, PagesProperties pagesProperties,
                              UserPrincipalAccess userPrincipalAccess) {
        this.commentDao = commentDao;
        this.userDao = userDao;
        this.newsDao = newsDao;
        this.auditService = auditService;
        this.commentDTOConverter = commentDTOConverter;
        this.paginationHalper = paginationHalper;
        this.pagesProperties = pagesProperties;
        this.userPrincipalAccess = userPrincipalAccess;
    }

    @Override
    @Transactional
    public void createComment(Long newsId, String content) {
        try {
            UserPrincipal userPrincipal = userPrincipalAccess.getUserPrincipal();
            User user = userDao.findOne(userPrincipal.getId());
            News news = newsDao.findOne(newsId);
            LocalDateTime currenrTime = LocalDateTime.now();
            Instant instant = currenrTime.atZone(ZoneId.systemDefault()).toInstant();
            Date date = Date.from(instant);
            Comment comment = new Comment();
            comment.setContent(content);
            comment.setUser(user);
            comment.setCreated(date);
            news.getComments().add(comment);
            comment.setNews(news);
            commentDao.create(comment);
            auditService.createEvent(user, "Create comment for news " + news.getTitle(), EventType.COMMENTS);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Integer> getPages(int currentPage) {
        try {
            Long countOfComments = commentDao.getCount();
            List<Integer> pages = paginationHalper.getPages(currentPage, countOfComments, pagesProperties.getCountOfCommentsOnPage());
            if (pages != null) {
                return pages;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.emptyList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<CommentDTO> getNewsCommentsInPage(Long newsId, int currentPage) {
        try {
            int startOfPage = paginationHalper.getStartOfPage(currentPage, pagesProperties.getCountOfCommentsOnPage());
            List<Comment> comments = commentDao.getCommentsOnPage(newsId, startOfPage, pagesProperties.getCountOfAuditsInPage());
            if (comments != null) {
                return commentDTOConverter.toDTOList(comments);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.emptyList();
    }

    @Override
    @Transactional
    public void deleteComment(Long commentId) {
        try {
            Comment comment = commentDao.findOne(commentId);
            if (comment != null){
                commentDao.delete(comment);
                User user = userDao.findOne(userPrincipalAccess.getUserPrincipalId());
                auditService.createEvent(user, user.getEmail() + " delete comment of " + comment.getUser().getEmail(), EventType.COMMENTS);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }
}
