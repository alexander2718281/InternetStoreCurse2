package ru.mail.alexanderkurlovich3.service.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.alexanderkurlovich3.dao.model.EventType;
import ru.mail.alexanderkurlovich3.dao.model.News;
import ru.mail.alexanderkurlovich3.dao.model.User;
import ru.mail.alexanderkurlovich3.service.AuditService;
import ru.mail.alexanderkurlovich3.dao.NewsDao;
import ru.mail.alexanderkurlovich3.service.NewsService;
import ru.mail.alexanderkurlovich3.dao.UserDao;
import ru.mail.alexanderkurlovich3.service.converter.impl.NewsDTOConwerter;
import ru.mail.alexanderkurlovich3.model.*;
import ru.mail.alexanderkurlovich3.exceptions.ServiceException;
import ru.mail.alexanderkurlovich3.service.properties.PagesProperties;
import ru.mail.alexanderkurlovich3.service.util.PaginationHalper;
import ru.mail.alexanderkurlovich3.service.util.UserPrincipalAccess;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    private static final Logger logger = LogManager.getLogger(NewsServiceImpl.class);
    private final NewsDTOConwerter newsDTOConwerter;
    private final NewsDao newsDao;
    private final UserDao userDao;
    private final AuditService auditService;
    private final PaginationHalper paginationHalper;
    private final PagesProperties pagesProperties;
    private final UserPrincipalAccess userPrincipalAccess;

    @Autowired
    public NewsServiceImpl(NewsDTOConwerter newsDTOConwerter, NewsDao newsDao, UserDao userDao, AuditService auditService, PaginationHalper paginationHalper, PagesProperties pagesProperties, UserPrincipalAccess userPrincipalAccess) {
        this.newsDTOConwerter = newsDTOConwerter;
        this.newsDao = newsDao;
        this.userDao = userDao;
        this.auditService = auditService;
        this.paginationHalper = paginationHalper;
        this.pagesProperties = pagesProperties;
        this.userPrincipalAccess = userPrincipalAccess;
    }


    @Override
    @Transactional
    public void createNews(NewsDTO newsDTO) {
        try {
            User user = userDao.findOne(userPrincipalAccess.getUserPrincipalId());
            News news = new News();
            news.setContent(newsDTO.getContent());
            news.setTitle(newsDTO.getTitle());
            news.setUser(user);
            LocalDateTime currenrTime = LocalDateTime.now();
            Instant instant = currenrTime.atZone(ZoneId.systemDefault()).toInstant();
            Date date = Date.from(instant);
            news.setCreated(date);
            newsDao.create(news);
            auditService.createEvent(user, "create news about " + news.getTitle(), EventType.NEWS);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Integer> getPages(int currentPage) {
        try {
            Long countOfNews = newsDao.getCount();
            List<Integer> pages = paginationHalper.getPages(currentPage, countOfNews, pagesProperties.getCountOfNewsOnPage());
            if (pages != null) {
                return pages;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.emptyList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<NewsDTO> getNewsInPage(int currentPage) {
        try {
            int startOfPage = paginationHalper.getStartOfPage(currentPage, pagesProperties.getCountOfNewsOnPage());
            List<News> news = newsDao.getNewsInPage(startOfPage, pagesProperties.getCountOfNewsOnPage());
            if (news != null) {
                List<NewsDTO> dtos = newsDTOConwerter.toDTOList(news);
                return dtos;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return Collections.EMPTY_LIST;
    }

    @Override
    @Transactional
    public void deleteNews(Long newsId) {
        try {
            News news = newsDao.findOne(newsId);
            newsDao.delete(news);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public NewsDTO getNewsById(Long newsId) {
        try {
            News news = newsDao.findOne(newsId);
            if (news != null) {
                NewsDTO newsDTO = newsDTOConwerter.toDTO(news);
                return newsDTO;
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return new NewsDTO();
    }

    @Override
    @Transactional
    public void updateNews(Long newsId, NewsDTO newsDTO) {
        try {
            News news = newsDao.findOne(newsId);
            news.setTitle(newsDTO.getTitle());
            news.setContent(newsDTO.getContent());
            Long userId = userPrincipalAccess.getUserPrincipalId();
            news.setUser(userDao.findOne(userId));
            newsDao.update(news);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }
}
