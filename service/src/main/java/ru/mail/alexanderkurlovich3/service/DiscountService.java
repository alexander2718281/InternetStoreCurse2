package ru.mail.alexanderkurlovich3.service;


import ru.mail.alexanderkurlovich3.model.DiscountDTO;

import java.util.List;

public interface DiscountService {

    void addDiscountToItem(Long itemId, Long discountId);

    List<DiscountDTO> getActualDiscounts();
}
