package ru.mail.alexanderkurlovich3.service;

import ru.mail.alexanderkurlovich3.model.AuditDTO;
import ru.mail.alexanderkurlovich3.dao.model.EventType;
import ru.mail.alexanderkurlovich3.dao.model.User;

import java.util.List;

public interface AuditService {

    void createEvent(long userId, String event, EventType type);

    void createEvent(User user, String event, EventType type);

    List<AuditDTO> getAuditsInPage(int currentPage);

    List<Integer> getPages(int currentPage);

}
