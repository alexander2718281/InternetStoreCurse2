package ru.mail.alexanderkurlovich3.service;


import ru.mail.alexanderkurlovich3.model.CommentDTO;

import java.util.List;

public interface CommentService {

    void createComment(Long newsId, String content);

    List<Integer> getPages(int currentPage);

    List<CommentDTO> getNewsCommentsInPage(Long newsId, int currentPage);

    void deleteComment(Long commentId);
}
