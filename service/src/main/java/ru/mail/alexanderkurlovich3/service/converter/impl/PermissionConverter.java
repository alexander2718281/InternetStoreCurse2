package ru.mail.alexanderkurlovich3.service.converter.impl;

import org.springframework.stereotype.Component;
import ru.mail.alexanderkurlovich3.service.converter.Converter;
import ru.mail.alexanderkurlovich3.service.converter.DTOConverter;
import ru.mail.alexanderkurlovich3.dao.model.Permission;
import ru.mail.alexanderkurlovich3.model.PermissionDTO;

@Component
public class PermissionConverter implements Converter<PermissionDTO, Permission>, DTOConverter<PermissionDTO, Permission> {

    @Override
    public Permission toEntity(PermissionDTO dto) {
        Permission permission = new Permission();
        permission.setId(dto.getId());
        permission.setName(dto.getName());
        return permission;
    }

    @Override
    public PermissionDTO toDTO(Permission entity) {
        PermissionDTO permissionDTO = new PermissionDTO(entity.getId(), entity.getName());
        return permissionDTO;
    }

}
