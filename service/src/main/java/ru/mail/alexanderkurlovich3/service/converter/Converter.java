package ru.mail.alexanderkurlovich3.service.converter;

import java.util.ArrayList;
import java.util.List;

public interface Converter<D, E> {

    E toEntity(D dto);
    default List<E> toEntityList(List<D> dtos){
        List<E> entyties = new ArrayList<E>();
        for (D dto : dtos) {
            entyties.add(toEntity(dto));
        }
        return entyties;
    }
}
