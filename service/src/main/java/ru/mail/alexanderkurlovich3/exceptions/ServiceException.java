package ru.mail.alexanderkurlovich3.exceptions;


public class ServiceException extends RuntimeException {

    private String message;

    public ServiceException(Exception e) {
        super(e);
    }

    public ServiceException(String messege, Exception e) {
        super(e);
    }

    @Override
    public String getMessage() {
        return message;
    }
}
