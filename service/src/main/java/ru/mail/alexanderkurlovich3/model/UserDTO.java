package ru.mail.alexanderkurlovich3.model;

public class UserDTO {

    private long id;
    private String surename;
    private String name;
    private String email;
    private String type;

    public UserDTO() {
    }

    public UserDTO(long id, String surename, String name, String email, String type) {
        this.id = id;
        this.surename = surename;
        this.name = name;
        this.email = email;
        this.type = type;
    }

    public String getSurename() {
        return surename;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getType() {
        return type;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", surename='" + surename + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
