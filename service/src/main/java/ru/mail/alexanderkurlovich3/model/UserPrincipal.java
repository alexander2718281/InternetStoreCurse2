package ru.mail.alexanderkurlovich3.model;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.*;
import ru.mail.alexanderkurlovich3.dao.model.Permission;
import ru.mail.alexanderkurlovich3.dao.model.User;

import java.util.Collection;
import java.util.List;

public class UserPrincipal implements org.springframework.security.core.userdetails.UserDetails {

    private Long id;
    private String email;
    private String password;
    private boolean isEnable;
    private List<GrantedAuthority> authorities;

    public UserPrincipal(User user){
        this.id = user.getId();
        this.email = user.getEmail();
        this.password = user.getPassword();
        this.isEnable = user.isEnable();
        String[] authorityList = user.getRole().getPermissions().stream()
                .map(Permission::getName)
                .toArray(String[]::new);
        this.authorities = AuthorityUtils.createAuthorityList(authorityList);
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isEnable;
    }

    public Long getId() {
        return id;
    }
}

