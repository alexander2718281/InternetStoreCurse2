package ru.mail.alexanderkurlovich3.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class ItemDTO {

    private Long id;
    private String uniqueNumber;
    private String name;
    private String description;
    private BigDecimal price;
    private BigDecimal priceWithDiscount;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUniqueNumber() {
        return uniqueNumber;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setUniqueNumber(String uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    public BigDecimal getPriceWithDiscount() {
        return priceWithDiscount;
    }

    public void setPriceWithDiscount(BigDecimal priceWithDiscount) {
        this.priceWithDiscount = priceWithDiscount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ItemDTO{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", price=").append(price);
        sb.append(", uniqueNumber='").append(uniqueNumber).append('\'');
        sb.append(", priceWithDiscount=").append(priceWithDiscount);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemDTO that = (ItemDTO) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(price, that.price) &&
                Objects.equals(uniqueNumber, that.uniqueNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, uniqueNumber);
    }
}
