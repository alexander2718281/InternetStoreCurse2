package ru.mail.alexanderkurlovich3.model;

import java.util.Date;

public class NewsDTO {

    private Long id;
    private String title;
    private String content;
    private Date created;
    private String userEmail;

    public NewsDTO() {
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Date getCreated() {
        return created;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String usetEmail) {
        this.userEmail = usetEmail;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NewsDTO{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append(", created=").append(created);
        sb.append(", usetEmail='").append(userEmail).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
