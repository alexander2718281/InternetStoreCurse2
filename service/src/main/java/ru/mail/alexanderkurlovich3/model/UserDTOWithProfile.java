package ru.mail.alexanderkurlovich3.model;


import java.util.Objects;

public class UserDTOWithProfile {

    private Long id;
    private String email;
    private String password;
    private String surename;
    private String name;
    private String telephone;
    private String address;
    private String type;
    private boolean enable;

    public UserDTOWithProfile() {
    }

    public String getPassword() {
        return password;
    }

    public String getSurename() {
        return surename;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getAddress() {
        return address;
    }

    public String getType() {
        return type;
    }

    public void setSurename(String surename) {
        this.surename = surename;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserDTOWithProfile{");
        sb.append("id=").append(id);
        sb.append(", email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", surename='").append(surename).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", telephone='").append(telephone).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", isEnable=").append(enable);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTOWithProfile that = (UserDTOWithProfile) o;
        return enable == that.enable &&
                Objects.equals(id, that.id) &&
                Objects.equals(email, that.email) &&
                Objects.equals(password, that.password) &&
                Objects.equals(surename, that.surename) &&
                Objects.equals(name, that.name) &&
                Objects.equals(telephone, that.telephone) &&
                Objects.equals(address, that.address) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, password, surename, name, telephone, address, type, enable);
    }
}
