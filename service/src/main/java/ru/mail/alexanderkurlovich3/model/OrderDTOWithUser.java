package ru.mail.alexanderkurlovich3.model;

import java.math.BigDecimal;
import java.util.Date;

public class OrderDTOWithUser {

    private Long id;
    private String itemName;
    private String userEmail;
    private int quantity;
    private Date created;
    private String status;
    private BigDecimal summ;
    private BigDecimal summWithDiscount;


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSumm() {
        return summ;
    }

    public void setSumm(BigDecimal summ) {
        this.summ = summ;
    }

    public BigDecimal getSummWithDiscount() {
        return summWithDiscount;
    }

    public void setSummWithDiscount(BigDecimal summWithDiscount) {
        this.summWithDiscount = summWithDiscount;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OrderDTOWithUser{");
        sb.append("id=").append(id);
        sb.append(", userEmail='").append(userEmail).append('\'');
        sb.append(", itemName='").append(itemName).append('\'');
        sb.append(", quantity=").append(quantity);
        sb.append(", created=").append(created);
        sb.append(", status='").append(status).append('\'');
        sb.append(", summ=").append(summ);
        sb.append(", summWithDiscount=").append(summWithDiscount);
        sb.append('}');
        return sb.toString();
    }
}
