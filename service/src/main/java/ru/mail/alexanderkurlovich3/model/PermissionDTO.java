package ru.mail.alexanderkurlovich3.model;


import java.util.Objects;

public class PermissionDTO {

    private long id;
    private String name;

    public PermissionDTO(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PermissionDTO that = (PermissionDTO) o;
        return id == that.id &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        String sb = "PermissionDTO{" + "id=" + id +
                ", name='" + name + '\'' +
                '}';
        return sb;
    }
}
