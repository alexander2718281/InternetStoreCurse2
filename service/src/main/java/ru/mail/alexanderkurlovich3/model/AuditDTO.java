package ru.mail.alexanderkurlovich3.model;

import java.util.Date;
import java.util.Objects;

public class AuditDTO {

    private Long id;
    private String event;
    private Date created;
    private String userEmail;
    private String type;




    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AuditDTO{");
        sb.append("id=").append(id);
        sb.append(", event='").append(event).append('\'');
        sb.append(", created=").append(created);
        sb.append(", userEmail='").append(userEmail).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuditDTO auditDTO = (AuditDTO) o;
        return Objects.equals(id, auditDTO.id) &&
                Objects.equals(event, auditDTO.event) &&
                Objects.equals(created, auditDTO.created) &&
                Objects.equals(userEmail, auditDTO.userEmail) &&
                Objects.equals(type, auditDTO.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, event, created, userEmail, type);
    }
}
