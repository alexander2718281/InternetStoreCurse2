package ru.mail.alexanderkurlovich3.model;

import java.util.Date;

public class DiscountDTO {

    private Long id;
    private String name;
    private int percent;
    private Date finalDate;

    public DiscountDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DiscountDTO{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", percent=").append(percent);
        sb.append(", finalDate=").append(finalDate);
        sb.append('}');
        return sb.toString();
    }


}
